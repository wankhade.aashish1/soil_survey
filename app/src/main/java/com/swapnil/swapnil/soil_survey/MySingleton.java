package com.swapnil.swapnil.soil_survey;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by swapnil on 13/4/19.
 */

public class MySingleton {

    private static  MySingleton mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;


    private MySingleton(Context context)
    {
        mCtx=context;
        requestQueue=getRequestQueue();
    }

    public  static synchronized  MySingleton getInstance(Context context)
    {
        if(mInstance==null)
        {
            mInstance=new MySingleton(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {

        if(requestQueue==null)
        {
            requestQueue= Volley.newRequestQueue(mCtx.getApplicationContext());
        }

        return requestQueue;
    }


    public <T> void addTorequestque(Request<T> request)
    {
        requestQueue.add(request);
    }
}
