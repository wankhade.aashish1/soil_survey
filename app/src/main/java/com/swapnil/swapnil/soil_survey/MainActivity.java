package com.swapnil.swapnil.soil_survey;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.swapnil.swapnil.soil_survey.Soil_Info.verifyStoragePermissions;


public class MainActivity extends AppCompatActivity {

    String default_username="soil";
    String default_password="survey";
    private EditText username;
    private EditText password;
    private Button login;
    RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

     username=(EditText)findViewById(R.id.username);
     password=(EditText)findViewById(R.id.password);
     login=(Button)findViewById(R.id.login);

//        username.setText("soil");
//        password.setText("survey");
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validate(username.getText().toString(),password.getText().toString());

            }
        });


    }

//    private void validate(String Username,String Userpassword)
//    {
//        if(Username.equalsIgnoreCase("soil")  && Userpassword.equalsIgnoreCase("survey"))
//        {
//            Intent intent=new Intent(MainActivity.this,Soil_Info.class);
//            startActivity(intent);
//        }
//
//        else
//        {
//            Toast.makeText(MainActivity.this, "कृपया वैध वापरकर्तानाव आणि पासवर्ड प्रविष्ट करा", Toast.LENGTH_SHORT).show();
//        }
//    }
    public void validate(String username, String password){
//        Toast.makeText(MainActivity.this, "Validating...", Toast.LENGTH_SHORT).show();
        verifyStoragePermissions(this);

        if(username.isEmpty() || password.isEmpty()){
            Toast.makeText(MainActivity.this, "कृपया वैध वापरकर्तानाव आणि पासवर्ड प्रविष्ट करा", Toast.LENGTH_SHORT).show();
        }else{
            JSONObject obj = new JSONObject();
            try{
                obj.put("mob", username);
                obj.put("pass", password);
                obj.put("secret","mp0z6ha6gnvxlAJhGJ");
            }catch (JSONException e){
                e.printStackTrace();
            }


            RequestQueue queue = Volley.newRequestQueue(this);
            String URL =  "http://api-ffs.mahapocra.gov.in/3rd-party/authService/sso";
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,URL,obj,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("login", response.toString());
                            try{
                                if(response.get("response").equals("Login success")){
                                    JSONObject data = (JSONObject) response.get("data");
                                    String  mobile=(String)data.get("mobile");
                                    String fname=(String)data.get("first_name");
                                    String mname=(String)data.get("middle_name");
                                    String lname=(String)data.get("last_name");
                                    String fullname=fname+" "+mname+" "+lname;
                                    JSONArray village_data_arr= (JSONArray) data.get("village_data");
                                    String alloted_census[] = new String[village_data_arr.length()];
                                    for(int i=0;i<village_data_arr.length();i++){
                                        JSONObject village_data = (JSONObject) village_data_arr.get(i);
                                        alloted_census[i] = (String) village_data.get("code");
                                    }

//                                    String census_code =  (String)village_data.get("code");
                                    Intent intent=new Intent(MainActivity.this,Soil_Info.class);
                                    intent.putExtra("mobile", mobile);
                                    intent.putExtra("fullname",fullname);
                                    intent.putExtra("census_code", alloted_census);
                                    startActivity(intent);
                                }else if(response.get("response").equals("Unautherized access")){
                                    Toast.makeText(MainActivity.this, "Invalid Username/Password", Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                            Log.d("volley error", error.toString());
                            Toast.makeText(getApplicationContext(), "Network ERROR: "+error.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
            queue.add(jsObjRequest);
        }
    }


}
