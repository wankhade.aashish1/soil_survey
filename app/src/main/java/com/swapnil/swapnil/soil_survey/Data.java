package com.swapnil.swapnil.soil_survey;

import java.math.BigInteger;

/**
 * Created by swapnil on 18/4/19.
 */

public class Data {

    String district_name,taluka_name,village_name,farmer_name,contact_no,gat_no,crop_name_kharif, crop_name_rabi, crop_name_other,landuse,soil_type,year,watering_type_kharif, watering_type_rabi, watering_type_other, image1, image2, uploaded_by_uname, uploaded_by_fullname;
    double soil_depth,yield_kharif,yield_rabi,yield_other;

    int watering_kharif, watering_rabi, watering_other;
    double latitude,longitude ;
    String time_info;

    public Data(String district_name, String taluka_name, String village_name, String farmer_name, String contact_no, String gat_no, String crop_name_kharif, String crop_name_rabi, String crop_name_other, String landuse, String soil_type, String year, String watering_type_kharif, String watering_type_rabi, String watering_type_other, double soil_depth, double yield_kharif,double yield_rabi,double yield_other, int watering_kharif, int watering_rabi, int watering_other, double latitude, double longitude, String time_info, String img1name, String img2name, String uploaded_by_uname, String uploaded_by_fullname) {
        this.district_name = district_name;
        this.taluka_name = taluka_name;
        this.village_name = village_name;
        this.farmer_name = farmer_name;
        this.contact_no = contact_no;
        this.gat_no = gat_no;
        this.crop_name_kharif = crop_name_kharif;
        this.crop_name_rabi = crop_name_rabi;
        this.crop_name_other = crop_name_other;
        this.landuse = landuse;
        this.soil_type = soil_type;
        this.year = year;
        this.watering_type_kharif = watering_type_kharif;
        this.watering_type_rabi = watering_type_rabi;
        this.watering_type_other = watering_type_other;
        this.soil_depth = soil_depth;
        this.yield_kharif = yield_kharif;
        this.yield_rabi=yield_rabi;
        this.yield_other=yield_other;
        this.watering_kharif = watering_kharif;
        this.watering_rabi = watering_rabi;
        this.watering_other = watering_other;
        this.latitude = latitude;
        this.longitude = longitude;
        this.time_info = time_info;
        this.image1 = img1name;
        this.image2 = img2name;
        this.uploaded_by_uname=uploaded_by_uname;
        this.uploaded_by_fullname=uploaded_by_fullname;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getTaluka_name() {
        return taluka_name;
    }

    public void setTaluka_name(String taluka_name) {
        this.taluka_name = taluka_name;
    }

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getGat_no() {
        return gat_no;
    }

    public void setGat_no(String gat_no) {
        this.gat_no = gat_no;
    }

    public String getCrop_name() {
        return crop_name_kharif;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name_kharif = crop_name;
    }

    public String getLanduse() {
        return landuse;
    }

    public void setLanduse(String landuse) {
        this.landuse = landuse;
    }

    public String getSoil_type() {
        return soil_type;
    }

    public void setSoil_type(String soil_type) {
        this.soil_type = soil_type;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

//    public String getWatering_type() {
//        return watering_type;
//    }

//    public void setWatering_type(String watering_type) {
//        this.watering_type = watering_type;
//    }

//    public int getSoil_depth() {
//        return soil_depth;
//    }

    public void setSoil_depth(int soil_depth) {
        this.soil_depth = soil_depth;
    }

    //public int getYield() {
      //  return yield;
    //}

   // public void setYield(int yield) {
      //  this.yield = yield;
    //}

//    public int getWatering() {
//        return watering;
//    }

//    public void setWatering(int watering) {
//        this.watering = watering;
//    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTime_info() {
        return time_info;
    }

    public void setTime_info(String time_info) {
        this.time_info = time_info;
    }

    //    public Data(String district_name, String taluka_name, String village_name, String farmer_name, String contact_no, String gat_no, String crop_name, String landuse, String soil_type, int soil_depth, double latitude, double longitude, String time_info) {
//        this.district_name = district_name;
//        this.taluka_name = taluka_name;
//        this.village_name = village_name;
//        this.farmer_name = farmer_name;
//        this.contact_no = contact_no;
//        this.gat_no = gat_no;
//        this.crop_name = crop_name;
//        this.landuse = landuse;
//        this.soil_type = soil_type;
//        this.soil_depth = soil_depth;
//        this.latitude = latitude;
//        this.longitude = longitude;
//        this.time_info = time_info;
//    }
//
//    public String getDistrict_name() {
//        return district_name;
//    }
//
//    public void setDistrict_name(String district_name) {
//        this.district_name = district_name;
//    }
//
//    public String getTaluka_name() {
//        return taluka_name;
//    }
//
//    public void setTaluka_name(String taluka_name) {
//        this.taluka_name = taluka_name;
//    }
//
//    public String getVillage_name() {
//        return village_name;
//    }
//
//    public void setVillage_name(String village_name) {
//        this.village_name = village_name;
//    }
//
//    public String getFarmer_name() {
//        return farmer_name;
//    }
//
//    public void setFarmer_name(String farmer_name) {
//        this.farmer_name = farmer_name;
//    }
//
//    public String getContact_no() {
//        return contact_no;
//    }
//
//    public void setContact_no(String contact_no) {
//        this.contact_no = contact_no;
//    }
//
//    public String getGat_no() {
//        return gat_no;
//    }
//
//    public void setGat_no(String gat_no) {
//        this.gat_no = gat_no;
//    }
//
//    public String getCrop_name() {
//        return crop_name;
//    }
//
//    public void setCrop_name(String crop_name) {
//        this.crop_name = crop_name;
//    }
//
//    public String getLanduse() {
//        return landuse;
//    }
//
//    public void setLanduse(String landuse) {
//        this.landuse = landuse;
//    }
//
//    public String getSoil_type() {
//        return soil_type;
//    }
//
//    public void setSoil_type(String soil_type) {
//        this.soil_type = soil_type;
//    }
//
//    public int getSoil_depth() {
//        return soil_depth;
//    }
//
//    public void setSoil_depth(int soil_depth) {
//        this.soil_depth = soil_depth;
//    }
//
//    public double getLatitude() {
//        return latitude;
//    }
//
//    public void setLatitude(double latitude) {
//        this.latitude = latitude;
//    }
//
//    public double getLongitude() {
//        return longitude;
//    }
//
//    public void setLongitude(double longitude) {
//        this.longitude = longitude;
//    }
//
//    public String getTime_info() {
//        return time_info;
//    }
//
//    public void setTime_info(String time_info) {
//        this.time_info = time_info;
//    }
}
