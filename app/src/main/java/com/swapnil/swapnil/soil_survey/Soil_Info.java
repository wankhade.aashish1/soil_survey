package com.swapnil.swapnil.soil_survey;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.StringBody;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import fr.ganfra.materialspinner.MaterialSpinner;

import static java.lang.Math.E;

public class Soil_Info extends AppCompatActivity {


    private static final int REQUEST_PERMISSIONS = 100;
    boolean boolean_permission;
    Double latitude, longitude;
    Geocoder geocoder;
    private EditText Lat, Longit, TimeStmp, Farmer_name_1, Contact_no, Gat_no, Soil_depth, yield, yield_rabi, yield_other;
    private ProgressBar progress, progress1;
    private RadioGroup radioGroup;
    SharedPreferences mPref;
    SharedPreferences.Editor medit;
    private Button gpsbutton, savebutton, submitbutton;
    MaterialSpinner district, taluka, village, soil_type, crop_type, crop_type_rabi, crop_type_other, landuse_type, year, watering_type, watering, watering_kharif, watering_type_kharif, watering_rabi, watering_type_rabi, watering_other, watering_type_other, Farmer_name;


    public static final String MyPREFERENCES = "MyPrefs";
    public static final String SelDistrict = "seldistrict";
    public static final String SelTaluka = "seltaluka";
    public static final String SelVillage = "selvillage";
    public static final String SelSoil = "selsoil";

    SharedPreferences sharedpreferences;

//    static final String JDBC_DRIVER = "org.postgresql.Driver";
//    static final String DB_URL = "jdbc:postgresql://localhost:5432/soil_survey";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

//    String URL = "http://www.cse.iitb.ac.in/jaltarang/soil_test_copy.php";
    String URL = "http://gis.mahapocra.gov.in/soil_survey/soil_test.php";
    AlertDialog.Builder builder;

//    String districtss[] = {"Select Soil Type","Clay Loam", "Clayey", "Gravelly Clay", "Gravelly Clay Loam", "Gravelly Loam", "Gravelly Sandy Clay Loam", "Gravelly Sandy Loam", "Gravelly Silty Clay", "Gravelly Silty Loam", "Loamy","Loamy Sand","Sandy","Sandy Clay", "Sandy Clay Loam", "Sandy Loam","Silty Clay", "Silty Clay Loam","Silty Loam"};

    String years[] = {
//            "-----", "2015", "2016", "2017", "2018", "2019", "2020"
            "2019"
    };

    String waterings[] = {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
    };

    String watering_types[] = {
            "-----",
            "ठिबक",
            "तुषार",
            "पाटपाणी"
    };

    String[] tal_in_dist, vil_in_tal;
    String districts[] =
            {

                    /*"जिल्हा निवडा",*/
                    "-----",
                    "अकोला",
                    "अमरावती",
                    "उस्मानाबाद",
                    "औरंगाबाद",
                    "जळगाव",
                    "जालना",
                    "नांदेड",
                    "परभणी",
                    "बीड",
                    "बुलडाणा",
                    "यवतमाळ",
                    "लातूर",
                    "वर्धा",
                    "वाशिम",
                    "हिंगोली"
            };

    String talukas[] = {
            /*"तालुका निवडा",*/
            "अकोट",
            "अकोला",
            "तेल्हारा",
            "पतुर",
            "बार्शी टाकळी",
            "बाळापूर",
            "मुर्तिजापूर",
            "अचलपूर",
            "अमरावती",
            "अंजनगाव",
            "चांदुर बाजार",
            "चांदुर रेल्वे",
            "चिखलदरा",
            "तिवसा",
            "दर्यापूर",
            "धामणगाव रेल्वे",
            "धारणी",
            "नांदगाव- खांडेश्वर",
            "भातकुली",
            "मोर्शी",
            "वरूड",
            "उमरगा",
            "उस्मानाबाद",
            "कळंब",
            "तुळजापूर",
            "परांडा",
            "भुम",
            "लोहारा",
            "वाशी",
            "औरंगाबाद",
            "कन्नड",
            "खुलताबाद",
            "गंगापूर",
            "पैठण",
            "फुलंब्री",
            "वैजापूर",
            "सिल्लोड",
            "सोयगाव",
            "अमळनेर",
            "एरंडोल",
            "चाळीसगाव",
            "चोपडा",
            "जळगाव",
            "जामनेर",
            "धरणगाव",
            "पाचोरा",
            "पारोळे",
            "बोदवड",
            "भडगाव",
            "भुसावळ",
            "मुक्ताईनगर",
            "यावल",
            "रावेर",
            "अंबड",
            "घनसांगवी",
            "जाफ़्राबाद",
            "जालना",
            "परतूर",
            "बदनापूर",
            "भोकरदन",
            "मंठा",
            "अर्धापूर",
            "उमरी",
            "किनवट",
            "कंधार",
            "देगलूर",
            "धर्माबाद",
            "नायगाव",
            "नांदेड",
            "बिलोली",
            "भोकर",
            "माहूर",
            "मुखेड",
            "मुदखेड",
            "लोहा",
            "हदगाव",
            "हिमायतनगत",
            "गंगाखेड",
            "जिंतूर",
            "परभणी",
            "पाथरी",
            "पालम",
            "पूर्णा",
            "मनवत",
            "सेलू",
            "सोनपेठ",
            "अंबेजोगाई",
            "आष्टी",
            "कैज",
            "गेवराई",
            "धारूर",
            "परळी",
            "पाटोदा",
            "बीड",
            "माजलगाव",
            "वडवणी",
            "शिरूर (कासार)",
            "खामगाव",
            "चिखली",
            "जळगाव (जामोद)",
            "देऊळगाव राजा",
            "नांदुरा",
            "बुलडाणा",
            "मलकापूर",
            "मेहकर",
            "मोताळा",
            "लोणार",
            "शेगाव",
            "सिंदखेड राजा",
            "संग्रामपूर",
            "अर्णी",
            "उमरखेड",
            "केळापूर",
            "घाटंजी",
            "झरी-जामनी",
            "दारव्हा",
            "दिग्रस",
            "नेर",
            "पुसद",
            "बाभुळगाव",
            "महागाव",
            "मारेगाव",
            "यवतमाळ",
            "राळेगाव",
            "वणी",
            "अहमदपूर",
            "उदगीर",
            "औसा",
            "चाकूर",
            "जळकोट",
            "देवणी",
            "निलंगा",
            "रेणापूर",
            "लातूर",
            "शिरूर- अनंतपाळ",
            "आर्वी",
            "करंजा",
            "देवळी",
            "वर्धा",
            "समुद्रपूर",
            "हिंगणघाट",
            "मानोरा",
            "मालेगाव",
            "मंगरूळपीर",
            "रीसोड",
            "वाशिम",
            "औंढा(नागनाथ)",
            "कळमनुरी",
            "बसमत",
            "सेनगाव",
            "हिंगोली",


    };

    String soil_types[] = {
            /*"माती निवडा",*/
            "-----",
            "हल्की",
            "मध्यम",
            "भारी"

    };
    String farmer_names[]={
            "testone",
            "testtwo"
    };
    String farmer_names_1[] = {
            "Akash gulab ubale",
            "Akash jalindar Ubale",
            "Amruta Kanhuji Sable",
            "Amruta kanoji sabale",
            "Ananda Gangaram Ubale",
            "Anil Gangaram Jadhav",
            "Anita Raghunath Bahirwal",
            "Anita Vilas Jadhav",
            "Anusaya Bhivaji Shinde",
            "Anusaya Rama Shinde",
            "Aravind m ubale ",
            "Arjun hanawata shinde",
            "Arjun Ravaji Kolekar",
            "Arvind Asaram ubale",
            "Asarabai Narayan bahirawal",
            "Asaram Bhimrao Kolekar",
            "Ashok Ashruba Wakale",
            "Ashrabai Narayan Bahirwal",
            "Ashruba Rajaram Sabale",
            "Ashruba Uttam Shinde",
            "Ashuba Lahanu Dandegaokar",
            "Asroba rajaram sable",
            "Baban Ramaji Ubale",
            "Babarao Nivrutti Khandare",
            "Baburao Kisanrao Rahate",
            "Bahagawan Baban ubale",
            "Balasaheb Sopan Kambale",
            "Bapurao Laxman Ubale",
            "Baynabai Ramkisan Kolekar",
            "Bebibai Vasanta Shinde",
            "Bhagawan Baban Ubale",
            "Bhagawat Appaji Naravade",
            "Sahebrav Bhagoji Rahate ",
            "Bhagorav Sheshrav Rhate",
            "Bhagwan Babn Ubale",
            "Bhagwat Tukaram Bangar",
            "Bhanodas Ramarao Bahirval",
            "Bhaurao Sakharam Kurhe",
            "Bhimashankar Bhanodas Bahirawal",
            "Bhimrao Dhodiba Gavhane",
            "Bhimrao Gangaram Ubale",
            "Bhimrav Raoji Koleker",
            "Bhujang Ganapat Sabale",
            "Bhujang Kisan chibhade",
            "Bhujangrao Laxmanrao Deshmukh",
            "Dadarao Babarav Hulgunde",
            "Dadarao Bhikaji Ubale",
            "Dadarao Ganpatrao Danegaonkar",
            "Damodar Laxman ubale",
            "Dattarao Narayanrao Pondhar",
            "Devidas R Kolekar",
            "Devidas Tukaram Kolekar",
            "Devling Talaya Kirtankar",
            "Devrao Namdev Ubale",
            "Dhananjay Tryambak Deshmukh",
            "Manik Dhondbarav Rahate ",
            "Dilip Prabhakar Shinde",
            "Dnyaneshwar Namdeo Narawade",
            "Ekanath Ramarao Bahirwal",
            "Gajanan Tryambakrao Deshmukh",
            "Ganesh Rajendra Sonatakke",
            "Gangaram Fakira Ubale",
            "Ganpat Sakharam Kurhe",
            "Ghanasham Sakharam Bahirawal",
            "Gitabai Jijeba Khandare",
            "Godavari Namdev Bahirawal",
            "Govindprasad Motilal Jethaliya",
            "Gulab Fakira Ubale",
            "Gulab Ganesh Naravade",
            "Gumfabai Rayaji Wakale",
            "Gunaji Jayavanta Sabale",
            "Hanuman Baban Ghoshir",
            "Harshavardhan Gulab Pradhan",
            "Jagannath Gopinath Ghoshir",
            "Janabai Sakharam Sabale",
            "Jijeba Maroti Khandare",
            "Kailas Bhagoji Jadhav",
            "Kailas Kisan Gavhane",
            "Kalavati Pralhad Shinde",
            "Kalpana Subhash Davhale",
            "Kamalakar Jijiba Khandare",
            "Kamalbai Devidas Kolekar",
            "Kashinath Arjun shinde",
            "Kashinath Hari Bhavu Shinde",
            "Keshav Bhiwaji Shinde",
            "Keshav Haribhavu Hattekar",
            "Keshav Nagnath Hattekar ",
            "Keshav Madhav Shinde",
            "Keshv Hari Hatekar",
            "Kisan Rambhau kurhe ",
            "Kusum Gangaram Ubale",
            "Lahanu Bhagoji Dandegaonkar",
            "Lahanuji Raghoji Dandegaonkar",
            "Lakshman kisan kurhe",
            "Latabai Jalindra Ubale",
            "Laxman  Kisan Kurhe",
            "Limbaji Tukaram kolekar ",
            "Madhav Bhivaji Shinde",
            "Madhav Haribhavu Hattekar ",
            "Ankush Madhav Hatekar ",
            "Madhav Kisan Uble",
            "Madhav Ramji Ubale",
            "Madhav Waman Sable",
            "Mahadu Kamaji Chibhade",
            "Mahadu Ramchandr Sable",
            "Mahanandabai Yashvatrao Ubale",
            "Mahesh Bhagorao rahate",
            "Manjula Gunaji Sabale",
            "Maroti Sahebrao Navaghare",
            "Muktabai Ramrao Rahate",
            "Nagesh Kisan Hattekar",
            "Namadev Sakharam sabale",
            "Namdev Sakharam Bahirawal",
            "Navnath Narayan Pondhar",
            "Nitin Ramarao rahate",
            "Pandhari Marotrao Rahate",
            "Pandit Ramrao Gavhane",
            "Pandit Vaman Sabale",
            "Parabatibai Lahanuji Dandegaonkar",
            "Parasram Punjaji Pradhan",
            "Parmeshwar Jijiba Ubale",
            "Prabhakar Bhivaji Shinde",
            "Prabhakar Devba Sable",
            "Prabhu Bhiwaji Shinde",
            "Prabhu Laxman Chibhade",
            "Prabhu Maroti Narwade",
            "Prabhu Punjaji Pradhan",
            "Prakash Jalindar Ubale",
            "Prakash Ucchitrao Sable",
            "Pralhad  Bhiwaji Shinde",
            "Pralhad Gyanbarao Magar",
            "Prayagbai Laxman Chibhade",
            "Radha Namdev Bahirwal",
            "Radhabai Kisan Ghanghav",
            "Raghunath Ramarao Bahirawal ",
            "Raghunath Ramrav Bahirawal",
            "Raghunath Uttam Shinde",
            "Rajkumar Jinjiba Ubale",
            "Raju Kisan Ubale",
            "Raju Laxman bahirawal",
            "Ramakisan Ravaji Kolekar",
            "Raman Mohnrav Ghoshir",
            "Ramarav Sahebarav Rahate",
            "Rameshwar Digambar Bhosale",
            "Ramji vittal Shinde ",
            "Ramkisn Babarao Hulgunde",
            "Ramrao Gulabrao Podhar",
            "Ranjana Anandrao Late",
            "Ranjana Rajkumar Ubale",
            "Ravindra Ekanath Bahirwal",
            "Sahebrao Fakira Ubale",
            "Sakharam Tulshiram Mudholkar",
            "Sakhubai Gangaram Jadhav",
            "Sambhaji Baliram Gavhane",
            "Sambhaji Jalba Ubale",
            "Sampa Ganuji Late",
            "Sandip Shivaji Dhangar",
            "Sandip Tukaram Ubale",
            "Sangita Navnath Pondhar",
            "Sanjay Uchitrav sable",
            "Sanjivani Baban Vankhede",
            "Sanjivani Madhav Hatekar",
            "Santosh Dadarao ulgunde",
            "Santosh Gyanabarao Magar",
            "Shantabai Devaba Sabale",
            "Shantabai Kisan Chibhade",
            "Shekurao Limbaji Kolekar",
            "Sheshrao Dattarao Devakar",
            "Sheshrav Ganesh Narwade",
            "Shidharth pandit ubale",
            "Shivaji Fakira Dhangar",
            "Shivaji Kisan Gavhane",
            "Shivaji Ukandi Patale",
            "Shiwaji Kisan Gavane",
            "Shriram Jayvanta Sabale",
            "Sidharth Pandit Ubale",
            "Sitabai Bhaurao Gavhane",
            "Sitaram Fakira Narwade",
            "Sitaram Kamaji Ubale",
            "Sonali Shivaji Gavane",
            "Subhadra Ashruba Wakale",
            "Subhash Damodhar Ubale",
            "Subhash Fakira Kolekar",
            "Subhash Rajendra Sontakke",
            "Subhash Ramrao Davhale",
            "Subhash Sudam Jadhav",
            "Sudam Bhagoji Jadhav",
            "Suresh Pannalal Mundada",
            "Sushila Baburav Sable",
            "Sushilabai Ashruba Wani",
            "Taibai Fakira Kolekar",
            "Taibai Uttam Shinde",
            "Tanhabai Shivaji Dhangar",
            "Tryambak Bhagoji Jadhav",
            "Tukaram kale uttam",
            "Tukaram Namdev Bahirawal",
            "Uchit Laxman Sabale",
            "Uttam Sudam Jadhav",
            "Uttam Tukaram Kale",
            "Vachhala Ramchandra Sable",
            "Vasanta Arjun Shinde",
            "Vasanta Laxmanrao Deshmukh",
            "Vishnu Kailas Narwade",
            "Vishwanath Arjun shinde",
            "Vishwanath Mahadev Dhangar",
            "Vittal lahanu dandegaonkar",
            "Vitthal Abhiman Kurhe",
            "Vitthal Limbaji Chibhade",
            "Vitthal Rama Dhote",
            "Vitthal Ramji Shinde",
            "Waman Sakharam Khure",
            "Yadav Ganapati Late",
            "Yuvraj Devidas kolekar"
    };

    String villages[] = {
            /*"गाव निवडा",*/
            "-----",
            "अगासखेड",
            "अल्यारपूर",
            "अंढ",
            "आलमपूर",
            "आलेगाव",
            "आलेवाडी",
            "आसेगाव",
            "इसापूर",
            "उमरा",
            "औरंगाबाद प्र.अडगाव",
            "करतवाडी आ.",
            "करोडी",
            "कवठा खुर्द",
            "कवठा बु.",
            "कवसा  खुर्द",
            "कवसा बुद्रुक",
            "कारतवाडी",
            "काळवाडी",
            "किंकखेड",
            "केळिवेली",
            "कौटसा",
            "खानापूर बुद्रुक",
            "खापरवाडी खुर्द",
            "खापरवाडी बुद्रुक",
            "खेरडा",
            "गारसोळी",
            "गिरजापूर",
            "ग्याजुद्दीन नगर",
            "चोहाट्टा",
            "जितापूर प्र.अडगाव",
            "जितापूर प्र.रुपागड",
            "जैनपूर पिंपरी",
            "जौळका",
            "जौळखेड खु्र्द",
            "जौळखेड बुद्रुक",
            "टाकळी खुर्द",
            "टाकळी बुद्रुक",
            "ठोकबर्डी",
            "तरोडा",
            "तांदूळवाडी",
            "दानोरी",
            "दिनोडा",
            "देउळगाव",
            "देवरी",
            "देवर्डा",
            "धनगरवाडी",
            "धागा",
            "धामणा बुद्रुक",
            "धारेळ",
            "नयनपुर",
            "नाखेगाव",
            "निजामपूर",
            "नेहोरी बुद्रुक",
            "पटोंडा",
            "परळा",
            "पळसोड",
            "पाटसूळ",
            "पिळकवाडी",
            "पिंपरी खुर्द",
            "पिंपरी डिक्कर",
            "पुंडा",
            "पेनोरी",
            "फत्तेपूर",
            "बांबर्डा बुद्रुक",
            "बेलुरा",
            "भोड",
            "मक्रामपूर",
            "मरोडा",
            "महमदपूर",
            "महालक्ष्मी",
            "मिर्झापूर",
            "मुंडगाव",
            "राजुरवाडी",
            "रीक्त",
            "रेळ",
            "रोहनखेड",
            "रौंडाळा",
            "लामकाणी",
            "लोटखेड",
            "वरूर",
            "वाणी",
            "वारूळा",
            "विटाळी",
            "शहापूर प्र अकोट",
            "शहापूर प्र.रुपागड",
            "सालखेड",
            "सावरखेड",
            "सावरगाव",
            "सावरा",
            "सांगवी",
            "सुल्तानपूर",
            "सोनबर्डी",
            "हनवाडी",
            "हिलालाबाद",
            "अगर",
            "अनकवाडी",
            "अन्वी",
            "अपोटी खु.",
            "अपोटी बु.",
            "अमंतापूर",
            "अलियाबाद",
            "अंबिकापूर",
            "आखातवाडा",
            "आपटापा",
            "उगवा",
            "एकलारा",
            "कत्यार",
            "कनदी",
            "कपिलेश्वर",
            "कळंबेश्वर",
            "काटी",
            "कासमपूर",
            "कासळी खु.",
            "कासळी बु.",
            "कांचनपूर",
            "कोळंबी",
            "कौलखेड गोमासे",
            "खडकी टाकळी",
            "खाकडी",
            "खाडका",
            "खानापुर",
            "खानापूर",
            "खारब खु.",
            "खारब बु.",
            "खांबोरा",
            "खोबरखेड",
            "गनोरी",
            "गांधीग्राम",
            "गोत्रा",
            "गोनापूर",
            "गोपाळखेड",
            "गोंडापूर",
            "घुसर",
            "घुसरवाडी",
            "चचोंडी",
            "चांदपूर",
            "चांदूर",
            "जलालपूर",
            "जलालबाद",
            "टाकळी जालम",
            "डोनवाडा",
            "ताकोडा",
            "तानखेड",
            "तारापूर",
            "तेलखेड",
            "दहीगाव",
            "दहीहंडा",
            "दापूरा",
            "दाबकी",
            "दाळंबी",
            "दुधाळम",
            "दुधाळा",
            "धामना",
            "धोतर्डी",
            "नवखेडा",
            "नवथळ",
            "निरट",
            "निरमलखेडा",
            "निंभोरा",
            "परीटवा़डा",
            "पळसो बु.",
            "पळीपाडा",
            "पळोधी",
            "पळोसा खु.",
            "पहाडपूर",
            "पाच पिंपळ",
            "पाटी",
            "फारमर्दाबाद",
            "बखराबाद",
            "बदलापूर",
            "बहादरपूर",
            "बहिरखेड",
            "बोंदरखेड",
            "भाम्हापूरी",
            "भोड",
            "भौरड",
            "मरोडी",
            "महदळापूर",
            "माजलापूर",
            "मांडळा",
            "मिर्झापुर",
            "मिर्झापूर",
            "मुजरे मोहमदपूर",
            "मुस्ताफपूर",
            "मोरगाव भाकरे",
            "म्हातोडी",
            "म्हैसापूर",
            "म्हैसंग",
            "यावळखेड",
            "राजापूर",
            "रामगाव",
            "रोहाना",
            "रंभापूर",
            "लाखोंडा खु.",
            "लाखोंडा बु.",
            "लोणी",
            "लोनाग्रा",
            "वडद खु.",
            "वडद बु.",
            "वनी",
            "वरुडी",
            "वल्लभ नगर",
            "वाकापूर",
            "वैराट राजापूर",
            "शमाबाद",
            "शाहपुर",
            "शिळोदा",
            "शेकापूर",
            "सांगवी खु.",
            "सांगवी बु.",
            "सांगवी मोहाडी",
            "सुकोडा",
            "सुलतान अजमपूर",
            "सोमठाणा",
            "संगळूद खु.",
            "संगळूद बु.",
            "संतोषपूर",
            "हतळा",
            "हिंगणा म्हैसपूर",
            "हिंगणी बु (दहीहंडा)",
            "हिंगमा तामसवाडी",
            "अकोली रुपराव",
            "अटकळी",
            "अडसूळ",
            "उकळी बाजार",
            "उबरखेड",
            "उमरशेवडी",
            "उम्री",
            "क-ही प्र.अडगाव",
            "कार्ला बु.",
            "काळेगाव",
            "खाकटा",
            "खापरखेड",
            "खेळकृष्णजी",
            "खेळदेशपांडे",
            "खेळ मुकादम",
            "खेळसातवजी",
            "गाडेगाव",
            "गोर्धा",
            "घोडेगाव",
            "चापनेर",
            "जस्तागाव",
            "झरी बाजार",
            "टाकळी",
            "तळेगाव प्र.पातुर्डी",
            "तळेगाव प्र.वडनेर",
            "तुडगाव",
            "दहीगाव",
            "दापूरा",
            "दौळा",
            "नरसीपूर",
            "निंबोरा खु.",
            "निंबोरा बु.",
            "निंबोळी",
            "नेर",
            "पाथर्डी",
            "पिवंदळ खु.",
            "पिवंदळ बु.",
            "पिंपरखेड",
            "बादखेड",
            "बाभुळगाव",
            "बामबर्डा खु.",
            "बेलखेड",
            "भांबेरी",
            "भोकर",
            "मानत्री खु.",
            "मानत्री बु.",
            "मानब्दा",
            "माळपूरा",
            "मोयपानी",
            "रानेगाव",
            "वडगाव रोठे",
            "वनगरगाव",
            "वरुड बु.",
            "वरुड वडनेर",
            "शिरसोळी",
            "सोनवाडी",
            "सौंदाळा",
            "हिवरखेड",
            "हिंगणी बु.",
            "अगीखेड",
            "अस्तूळ",
            "काकडदरी",
            "कोथारी खु.",
            "कोसगाव",
            "खानापूर",
            "खामखेड",
            "गोंधळवाडी",
            "चिखलखेड सस्ती",
            "जोगतलाव",
            "तांदळी खु.",
            "नांदखेड",
            "पार्डी",
            "बेलुरा खु.",
            "बेलुरा बु.",
            "भांडरज बु.",
            "माळराजूरा",
            "शिव",
            "सोतळवन",
            "अटकळी",
            "अश्करीपूर",
            "कान्हेरी",
            "जलालबाद",
            "जोगलखेड",
            "टिटवा",
            "तिवसा खु.",
            "तिवसा बु.",
            "दगडपारवा",
            "पिंपळ शेंडा",
            "पुनोटी खु.",
            "बागयत बार्शी टाकळी",
            "बार्शी टाकळी",
            "महागाव",
            "मिर्झापूर",
            "मंगरुळ",
            "राजन खेड",
            "राजनखेडतांडा (नविन)",
            "राजंदा",
            "रुस्तमबाद",
            "वरखेड",
            "वरखेड (वाघ)",
            "शिंदखेड",
            "सय्यदपूर",
            "हळदोली",
            "अंत्री मलकापूर",
            "अंदुरा",
            "उरळ खु.",
            "उरळ बु.",
            "कळन्बी महागाव",
            "कळंबा खु.",
            "कवठा",
            "काजीखेड",
            "कारंजा रमजानपूर",
            "खामखेड",
            "खार्बी",
            "खिरपूरी खु.",
            "खिरपूरी बु.",
            "खंडाला",
            "गैगाव",
            "जानोरीमैली",
            "झुरळ खु.",
            "झुरळ बु.",
            "टाकळी खुरेशी",
            "टाकळी खोजबड",
            "टाकळी निमकर्डा",
            "डोंगरगाव",
            "दगडखेड",
            "दधाम बु.",
            "देगाव",
            "नया अंदुरा",
            "नाकशी",
            "नागड",
            "नांदखेड",
            "निंबा",
            "निंबी",
            "बारलिंगा",
            "बाहदूरा",
            "बोरगाव वैराळे",
            "भोरटेक",
            "माळखेड",
            "माळवाडा",
            "मोखा",
            "मोरगाव सादीजन",
            "मोर्झडी",
            "मंजारी",
            "रीधोरा",
            "लोहारा",
            "वझेगाव",
            "व्यल्ला",
            "शिंगोळी",
            "सतारगाव",
            "सागड",
            "सावरपाटी",
            "सोनळा",
            "स्वरुपखेड",
            "हतरुण",
            "हता",
            "हसनापूर",
            "हिंगणशिकारी",
            "हिंगणा अडसूळ",
            "हिंगणा निंबा",
            "अटकळी",
            "अनभोरा",
            "अमतवाडा",
            "अल्लापूर",
            "उनखेड",
            "उमई",
            "औरंगपूर",
            "कवोथा खोलापूर",
            "काडवी",
            "कामठा",
            "कासवी",
            "कासारखेड",
            "किन्ही",
            "कुरुम",
            "कोळसरा",
            "कौठा सोपीनाथ",
            "खापरवाडा",
            "खारबडी",
            "खारब ढोरे",
            "खुडवंतपूर",
            "खोदड",
            "खंडाळा",
            "गाजीपूर",
            "गिर्धारपूर",
            "गुंजवाडा",
            "गोरेगाव",
            "गौळखेडी",
            "चिखली",
            "चिंचखेड",
            "चुंगशी",
            "जामठी खु.",
            "जामठी बु.",
            "जांभा खु.",
            "जांभा बु.",
            "जितापूर",
            "जेथापूर",
            "टाकळी गाजीपूर",
            "ताकवाडा",
            "तिपताळा",
            "तुरखेड",
            "दहातोंडा",
            "दाटवी",
            "दाताळा",
            "दापूरा",
            "दुर्गवाडा",
            "देवरान",
            "धानोरा पाटेकर",
            "धानोरा वैध्या",
            "नवसाळ",
            "नागठाणा",
            "निंभा",
            "पराड",
            "पिंगळा",
            "पोटा",
            "पोही",
            "फनी",
            "बल्ला खेड",
            "बहादूरपूर",
            "बापोरी",
            "बोर्ता",
            "ब्रम्ही खु.",
            "ब्राम्ही (बाई)",
            "ब्राम्ही बु.",
            "भागोरा",
            "भाटोरी",
            "मना",
            "मलकापूर",
            "मातोडा",
            "मुंगशी",
            "मोहबतपूर",
            "मंगरुळ कांबे",
            "यशवंतपूर",
            "येंदळी",
            "रसुलपूर",
            "रसुळपूर",
            "राजनापूर खिनखिनी",
            "राजूरा घाटे",
            "रामटेक",
            "रेपतखेड",
            "रोहाना",
            "रंभापूर",
            "लखपूरी",
            "लसनापूर",
            "लैट",
            "लोंसना",
            "लंघापूर",
            "वाघजळी",
            "वांतीपूर",
            "विरवाडा",
            "शिर्ताळा",
            "शेनी",
            "शेरवाडी",
            "शेलू नजिक",
            "शेलू बाजार",
            "शेलू वेताळ",
            "समशेरपूर",
            "सळातवाडा",
            "साखरी",
            "साहदतपूर",
            "सांगवा",
            "सांगवी",
            "सांजापूर",
            "सिर्सो",
            "सुलतानपूर",
            "सोनोरी",
            "सोनोरी (बोपरी)",
            "संजापूर",
            "हतगाव",
            "हसनापूर",
            "हिरपूर",
            "हिवरा कोर्डे",
            "चिखली",
            "जहानपूर",
            "निमखेडा",
            "बाग अंबडा",
            "बेलखेडा",
            "मेंगनाथपूर",
            "येळकी",
            "येसुर्ना",
            "राजूरा",
            "रीक्त",
            "वडूरा",
            "हिवरा",
            "अमदापूर",
            "अमळा",
            "अ-हाड",
            "अंगोडा",
            "अंतोरा",
            "उदखेड",
            "कात अमळा",
            "कापूसतळनी",
            "कामुंजा",
            "कु-हाड",
            "कुंड सर्जापूर",
            "केकटपूर",
            "गंगापूर",
            "चंगापूर",
            "दस्तापूर",
            "धानोरा कोकाटे",
            "नया अकोला",
            "नांदुरा पिंगळई",
            "पार्डी",
            "पुसाडा",
            "फजलपूर",
            "बहीलोळपूर",
            "ब्राम्हण वाडा गोविंदपूर",
            "ब्राम्हणवाडा भगत",
            "माळेगाव",
            "मोरंगना",
            "म्हासळा",
            "रसुळपूर",
            "रीक्त",
            "लोनटेक",
            "वडगाव जिरे",
            "वनर्शी",
            "वर्धी",
            "वळगाव",
            "शिराळा",
            "सुकळी",
            "अडगाव अडे",
            "एकलारा",
            "एवाजपूर",
            "औरंगपूर",
            "कमलापूर",
            "कसबेगव्हाण",
            "कापूसतळनी",
            "काळगव्हाण",
            "काळवाडा",
            "कुंभारगाव खु.",
            "कुंभारगाव बु.",
            "कोकर्डा",
            "कोतेगाव",
            "कोथा",
            "खानमपूर",
            "खासपूर",
            "खुडवनपूर",
            "खेल कोटात",
            "खैरगव्हाण",
            "गणेशपूर",
            "गावंडेगाव बु.",
            "घोडसगाव",
            "चिंचोली बु.",
            "जवळा खु.",
            "जवळा बु.",
            "डोंबाळा",
            "तारोडा",
            "देऊळगाव",
            "नारयणपूर",
            "निंभारी",
            "पजलपूर",
            "पार्डी",
            "पिंपळगव्हाण",
            "बोरगाव अंबाडा",
            "मलकापूर",
            "मलकापूर खु.",
            "मोहबतपूर",
            "रत्नापूर",
            "रत्नापूर जोगर्डा",
            "रामपूरा",
            "लाखनवाडी",
            "वनोजा",
            "वरुड खु.",
            "विहीगाव बु.",
            "समशेरपूर",
            "सराय",
            "सर्फाबाद",
            "साखरी",
            "सैदापूर",
            "सोनगाव",
            "हयापूर",
            "हिंगणी",
            "हुसेनपूर खोडगाव",
            "हंतोडा",
            "अखातवाडा",
            "अळीपूर",
            "असेगाव",
            "इनापूर",
            "काजळी",
            "कुरणखेड",
            "कृष्णापूर",
            "कोतगावंडी",
            "कोदोरी",
            "खारळा",
            "गोविंदपूर",
            "जगन्नाथपूर",
            "जाधवपूर",
            "जामपूर",
            "जासापूर",
            "टाकरखेडा",
            "तामसवाडी",
            "तुळजापूर गाधी",
            "त्रिमाळपूर",
            "दत्तापूर",
            "दहीगाव",
            "धानोरा",
            "नागरवाडी",
            "निमखेडा",
            "बहादरपूर",
            "बेलखेडा",
            "ब्राम्हणवाडा थडी",
            "ब्राम्हणवाडा पाठक",
            "मधान",
            "मलकापूर",
            "मसोड",
            "मुरदपूर",
            "मोहनगाव",
            "रसिदपूर",
            "रसुल्लापूर",
            "रहाटगाव",
            "राजना",
            "राजूरा (लहान)",
            "वडाळा",
            "वणी",
            "वथोंडा",
            "वाडुरा",
            "विथाळपूर",
            "शहापूर",
            "शिवपूर",
            "सर्फाबाद",
            "सिरजगाव बांड",
            "सुरळी",
            "सुलतानपूर",
            "सुंदरपूर",
            "हिपूर",
            "हुशंगाबाद",
            "हैदतपूर",
            "अजितपूर",
            "कवठा कडू",
            "खुडवंतपूर",
            "टाकळी",
            "दहीगाव",
            "दिघी कोल्हे",
            "बुधळी",
            "माळखेड",
            "लालखेड",
            "सावंगी संगम",
            "हडप्पा",
            "काटकुंभ",
            "कोत्मी",
            "कोयलारी",
            "गांगरखेडा",
            "झिंगापूर",
            "ढाकना",
            "तोरणवाडी",
            "दोमा",
            "नागरतास",
            "भागदरी",
            "भामदेही",
            "मेनघाट",
            "ठाणथुनी",
            "तळेगाव ठाकूर",
            "तेवोसा",
            "रीक्त",
            "वरखेड",
            "विनचोरी",
            "सुरवडी खु.",
            "सुरवडी बु.",
            "अजितपूर",
            "अडुळा",
            "अमळा",
            "अराळा",
            "अळमपूर",
            "अहमदपूर",
            "अंतरगाव",
            "आंतरगाव",
            "इचोरा",
            "इंदालवाडी",
            "ईटकी",
            "ईलोरी मिर्झापूर",
            "ईळीचपूर",
            "उपरई",
            "उमरी ईतबारपूर",
            "उमरी कुरण",
            "उमरी ममदाबाद",
            "एकलारा भामोड",
            "एरंडगाव",
            "कळमगव्हाण",
            "कातखेडा",
            "कान्होळी",
            "कापशी",
            "कारटखेड",
            "काळशी",
            "कासमपूर",
            "कुकसा",
            "कुबेरी",
            "कोळंबी",
            "खानपूर",
            "खारसांगलूर",
            "खालर",
            "खालिलपूर",
            "खिरगव्हाण",
            "खुर्माबाद",
            "खेळ नागवे",
            "खैरी",
            "गणेशपूर",
            "गाजीपूर",
            "गैवाडी",
            "गोलेगाव",
            "गौरखेडा",
            "घाडा",
            "घुईखेड",
            "घोडचंदी",
            "चांडोळा",
            "चांदई",
            "चांदखेड",
            "चांदूर",
            "चंद्रपूर",
            "जहानपूर",
            "जागरवाडी",
            "जासपूर",
            "जितापूर",
            "जैतापूर",
            "जैनपूर",
            "झिंगाळा",
            "टाकर खेडा कवडे",
            "टाकळी",
            "टोंगळा बाद",
            "डोंगरगाव",
            "तामसवाडी",
            "तेलखेडा",
            "थिलोरी",
            "दर्यापूर",
            "दारखेडा",
            "दारपूर",
            "दिघी",
            "धानोरा",
            "धामोडी",
            "नरदोडा",
            "नरसिंगपूर",
            "नळवाडा",
            "नाचोना",
            "नांदुरा",
            "नांदेड बु.",
            "नांद्रुन",
            "नैगाव",
            "पाथरविरा",
            "पानोरा",
            "पिंपळ खुटा",
            "पिंपळोद",
            "पेठ ईतबारपूर",
            "प्रल्हादपूर",
            "बहादरपूर",
            "बानोसा",
            "बाभळी",
            "बेलोरा",
            "बेंबाळा खु.",
            "बेंबाळा बु.",
            "बोराळा",
            "भामबोरा",
            "भामोड",
            "भुईखेडा",
            "भुजवाडा",
            "भुरस रामागड",
            "मलकापूर बु.",
            "महामदपूर",
            "महिमापूर",
            "महूळी",
            "मातरगाव",
            "मार्कंड",
            "म्हेसपूर",
            "म्हैसपूर",
            "येवोडा बु.",
            "राजखेड",
            "रामगाव",
            "रामतिर्थ",
            "रीक्त",
            "रुस्तमपूर",
            "लासूर",
            "लेहेगाव",
            "लोटवाडा",
            "लोधीपूर",
            "लोहितखेड",
            "वडनेर गांगई",
            "वडुरा",
            "वरुड बु.",
            "शहापूर",
            "शिरजदा",
            "शिवर खु.",
            "शिवरखेडा",
            "शिवर बु.",
            "शिंगणवाडी",
            "शिंगनापूर",
            "सागरवाडी",
            "सामडा",
            "सासन बु.",
            "सासन रामरापूर",
            "सांगकूड",
            "सांगवा खु.",
            "सांगवा बु.",
            "सुकळी",
            "सुजापूर",
            "सोनखस",
            "सोनखेड",
            "हसनापूर",
            "हिंगणी मिर्झापूर",
            "हेबतपूर",
            "अंजनसिंगी",
            "कवळी",
            "गव्हा निपानि",
            "चिंचपूर",
            "जळगाव",
            "जहानपूर",
            "तुळजापूर",
            "दाभाडा",
            "पिंपळखुटा",
            "मुंड व्यंकटेश त्रिंबक",
            "मुंड व्यंकटेश भास्कर",
            "वथोडा बु.",
            "वसड",
            "खापरखेडा",
            "खैरी",
            "गडगामळूर",
            "गौळणडोह",
            "झिळपी",
            "झिळंगपाटी",
            "तात्रा",
            "धोमनढाना",
            "पाथरपूर",
            "बिबामळ",
            "भोकरबर्डी",
            "मांगिया",
            "राजापूर",
            "रानपिसा",
            "रीक्त",
            "रंगूबेळी",
            "लक्तु",
            "सद्राबर्डी",
            "एरंडगाव",
            "दादापूर",
            "दाभा",
            "दुर्गापूर",
            "पाळा",
            "बेळोरा हिरापूर",
            "वळकी ",
            "वाकपूर",
            "शेंदणी",
            "अडवी",
            "अफजलपूर",
            "अबिटपूर",
            "अळणगाव",
            "अष्टी",
            "असरा",
            "अंचाळवाडी",
            "अंतापूर",
            "इंदापूर",
            "ईब्राहिमपूर",
            "ईस्माइलपूर",
            "उत्तमसरा",
            "उदापूर",
            "उमरटेक",
            "उमरापूर",
            "कळमगव्हाण",
            "कवठा",
            "कसमपूर",
            "काकर खेडा",
            "कामनापूर",
            "कुमागड",
            "कुंड खुर्द",
            "कृष्णापूर",
            "कोळटेक",
            "खातिजापूर",
            "खानापूर",
            "खारतळेगाव",
            "खाल्लर",
            "खाळखोनी",
            "खोलापूर",
            "गनोरी",
            "गायवाडी",
            "गोपगव्हाण",
            "गोविंदपूर",
            "गौरखेडा",
            "घाटखेडा",
            "चाकूर",
            "चांदपूर",
            "चुनकी",
            "चेचरवाडी",
            "जळका",
            "जानेवाडी",
            "जावरा",
            "जासपूर",
            "जैतापूर",
            "झांजी",
            "टाकरखेडा (सांभू)",
            "ढोलेवाडी",
            "तातरपूर",
            "तुळजापूर",
            "दगडगड",
            "दरारखेडा",
            "दर्याबाद",
            "दहातोंडा",
            "देगुरखेडा",
            "देवोरी",
            "धनगरखेडा",
            "धामोरी",
            "नवथाळ बु.",
            "नारायणपूर",
            "नावेद",
            "नांदेड खु.",
            "निमखेडा",
            "निरुळ गंगामाई",
            "निंदोरी",
            "निंभा",
            "परळम",
            "पोहरा",
            "बडेगाव",
            "बळीमरखेडा",
            "बोकुरखेडा",
            "बोरखेडी खु.",
            "बोरखेडी बु.",
            "बोंडेवाडी",
            "भाळसी",
            "मकरंदबाद",
            "मक्रमपूर",
            "मलकापूर",
            "मानखेडा",
            "मार्की",
            "मालपूर",
            "मालापूर",
            "म्हैसापूर",
            "रसुलपूर",
            "राजेगाव",
            "रामा",
            "रायपूर",
            "रीक्त",
            "रीनमोचन",
            "रुस्तमपूर",
            "वडाळा",
            "वाकी",
            "वाघोडा",
            "वातोंडा",
            "वाथोडा शुक्लेश्वर",
            "वासेवाडी",
            "विर्शी",
            "वैगाव",
            "वंदळी",
            "शिवापूर",
            "शेरपूर",
            "सरबालनपूर",
            "सरमासपूर",
            "सायत",
            "सावरखेडा",
            "सावूर",
            "सोनारखेडा",
            "संभेगाव",
            "हतखेडा",
            "हतुर्ना",
            "हरताळा",
            "हरतोटी",
            "हसनपूर",
            "हिम्मतपूर",
            "हिरापूर",
            "अश्तोळी",
            "कातपूर",
            "कोळविहीर",
            "खानपूर",
            "खोपडा",
            "जयमलपूर",
            "तारोडा",
            "दर्यापूर",
            "दोमक",
            "धामनगाव",
            "पोरगव्हाण",
            "बहिरामपूर",
            "भावसिंगपूर",
            "ममदापूर",
            "रायपूर",
            "लडकी बु.",
            "लष्करपूर",
            "लिहीदा",
            "वाघोली",
            "विश्नोरा",
            "शहानवाजपूर",
            "शिरखेड",
            "सुलतानपूर",
            "सोनगाव",
            "कारजगाव",
            "गोरेगाव",
            "धामनधस",
            "नागझिरी",
            "पळसोना",
            "पिंपालखुता",
            "बेनोडा",
            "मनकापूर",
            "मंगोना",
            "वाढोणा",
            "शेकडारी",
            "सावंगा",
            "ईकुर्गा",
            "ईकुर्गावाडी",
            "कदमपूर",
            "कद्दोरा",
            "कळदेव निंबाळा",
            "कळनिंबाळा",
            "कवठा",
            "कारळी",
            "कुन्हाळी",
            "केसर जवळगा",
            "कोरेगाव",
            "कोळसूर (के)",
            "कोळसूर (जी)",
            "गुगलगाव",
            "गुरुवाडी",
            "चिंचकोटा",
            "जगदाळवाडी",
            "जवळगा बेट",
            "ताळमोड",
            "त्रिकोळी",
            "थोरलीवाडी",
            "दुधनळ",
            "धाकटीवाडी",
            "नारंगवाडी",
            "बाबळसूर",
            "बाळसूर",
            "बोरी",
            "मळगी",
            "मळगीवाडी",
            "माडज",
            "मातोळा खु.",
            "येळी",
            "रामपूर",
            "वर्नाळवाडी",
            "वागदरी",
            "व्हंताळ",
            "सावळसूर",
            "सुपतगाव",
            "हिप्पर्गाराव",
            "हंद्रळ",
            "अनसुर्डा",
            "अंबेजवळगा",
            "अंबेवाडी",
            "अंबेहोळ",
            "उत्तमी कायापूर",
            "काकसपूर",
            "कारजीखेडा",
            "कावळदरा तांडा",
            "कोंड",
            "कोंबडवाडी",
            "कौडगाव (बावी)",
            "खानापूर",
            "गड देवदरी",
            "गोगाव",
            "गौंडगाव",
            "घातंग्री",
            "घुग्गी",
            "चिखली",
            "जुनोनी",
            "झारेगाव",
            "ढोकी",
            "तावरज खेडा",
            "तोरंबा",
            "देवळाली",
            "नांदुर्गा",
            "निताळी",
            "पळसवाडी",
            "पाटोडा",
            "बरमगाव खु.",
            "बरमगाव बु.",
            "बोरखेडा",
            "बोरगाव राजे",
            "भनसगाव",
            "भंडारी",
            "मेडसिंगा",
            "मेंढा",
            "रुई ढोकी",
            "रुईभार",
            "लासोना",
            "वखारवाडी",
            "वडगाव",
            "वडाळा",
            "वाळगूड",
            "शेकापूर",
            "समुद्रवणी",
            "सांगवी",
            "सांजा",
            "सोनेगाव",
            "अवड शिरपूरा",
            "अंदोरा",
            "ईकुर्का",
            "खडकी",
            "खामसवाडी",
            "गोविंदपूर",
            "गौर",
            "गौरगाव",
            "जावळा खु.",
            "ताडगाव",
            "तांदुळवाडी",
            "दिकसळ",
            "देवधानोरा",
            "नागझरवाडी",
            "नागुळगाव",
            "नैगाव",
            "बारमाचीवाडी",
            "बोरगाव बु.",
            "भोसा",
            "माळकारंजा",
            "मंगरुळ",
            "रीक्त",
            "वठावाडा",
            "वडगाव (जगीर)",
            "वाकडीकेज",
            "वानेवाडी",
            "शिरढोण",
            "शेलगाव दिवनी",
            "साटेफळ",
            "सौंदाना अंबा",
            "हवरगाव",
            "हसेगाव केज",
            "हसेगाव (शिरढोण)",
            "अपसिंगा",
            "अरबळी",
            "अंडूर",
            "ईटकळ",
            "उमरगा",
            "काक्रंबवाडी",
            "काक्रंबा",
            "कात्री",
            "कामठा",
            "कार्ला",
            "किळज",
            "कुनसावळी",
            "केरुर",
            "केशेगाव",
            "खडकी",
            "खंडाळा",
            "घांदोरा",
            "चिकुंद्रा",
            "चिवरी",
            "जळकोट",
            "जळकोटवाडी (नळ)",
            "जवळगा मेसई",
            "ताडवळा",
            "तुळजापूर",
            "तेलरनगर",
            "देवसिंगा (तुळ)",
            "देवसिंगा नळ",
            "धनगरवाडी",
            "धेक्री",
            "फुलवाडी",
            "बाभळगाव",
            "बारुळ",
            "बोरनडवाडी",
            "बोरनडवाडी (नळ)",
            "बोरी",
            "बोळेगाव",
            "मानमोडी",
            "मुर्ता",
            "मोरडा",
            "रीक्त",
            "वडगाव देवो",
            "वडगाव लाख",
            "वडाचा तांडा",
            "वनेगाव",
            "शिर्गापूर",
            "शिवकरवाडी",
            "शिंदगाव",
            "सराटी",
            "साळगरा तातूर",
            "साळगरा दिवटी",
            "हगंर्गा",
            "होनळा",
            "होर्टी",
            "हंगर्गा (नळ)",
            "हंगळूर",
            "अनाळा",
            "अळेश्वर",
            "अवर पिंपरी",
            "ईंगोदा",
            "उंडेगाव",
            "कपिलापूरी",
            "कात्राबाद",
            "कांगलगाव",
            "गोसावीवाडी (डोंजा)",
            "घारागाव",
            "चिंचपूर खु.",
            "जकाते वाडी",
            "जावळा (एन.)",
            "ताकमोडवाडी",
            "देवगाव खु.",
            "धागपिंपरी",
            "नळगाव",
            "बंगाळवाडी",
            "मलकापूर",
            "येनेगाव",
            "रत्नापूर",
            "लोणी",
            "लोहारा",
            "वडनेर",
            "वाटेफळ",
            "वाडी राजूरी",
            "वाणेगव्हाण",
            "शिराळा",
            "सावदरवाडी",
            "हिंगणगाव खु.",
            "हिंगणगाव बु.",
            "अष्टा",
            "अष्टेवाडी",
            "चिंचोली",
            "जांब",
            "दिंडोरी",
            "दुक्करवाडी",
            "देवळाली",
            "पन्हाळवाडी",
            "पाडोळी",
            "बावी",
            "भवानवाडी(बा)",
            "भुम (रुरल)",
            "भोंगिरी",
            "मात्रेवाडी",
            "रामकुंड",
            "रीक्त",
            "रोसांबा",
            "वरेवडगाव",
            "वाकड",
            "वांगी खु.",
            "वांगी बु.",
            "सुक्ता",
            "सोनगिरी",
            "हदोंगी",
            "हिवरा",
            "अचळेर",
            "अर्णी",
            "उंदरगाव",
            "कानेगाव",
            "कारवंजी",
            "कोळनूर पांडरी",
            "जेवळी",
            "दस्तापूर",
            "नागरळ",
            "फानेपूर",
            "बेलवाडी",
            "बेंडकळ",
            "माळेगाव",
            "मोघा खु.",
            "मोघा बु.",
            "रीक्त",
            "लोहारा खु.",
            "लोहारा बु.",
            "वडगाव",
            "वडगाववाडी",
            "विलासपूर पंढरी",
            "हिप्परगरवा",
            "हिप्पर्गा सय्यद",
            "इंदापूर",
            "ईसरुप",
            "कान्हेरी",
            "केळेवाडी",
            "खानापूर",
            "खामकरवाडी",
            "गोजवाडा",
            "गोलेगाव",
            "घाटपिंपरी",
            "घोडकी",
            "जेबा",
            "झिन्नर",
            "तेरखेडा",
            "दसमेगाव",
            "नांदगाव",
            "पार्डी",
            "बावी",
            "बोरी",
            "मांडवा",
            "यसावंडी",
            "रूई",
            "लोणखस",
            "वडजी",
            "सटवाईवाडी",
            "सोनारवाडा",
            "अडगाव बु.",
            "अपटगाव",
            "अब्दीमंडी",
            "अळमपूर",
            "अंजनडोह",
            "एकोड",
            "कारजगाव",
            "कुंभेफळ",
            "केसापूरी",
            "केसापूरी तांडा",
            "कोनेवाडी",
            "खामखेडा",
            "गारखेडा",
            "गांधेळी",
            "चर्था",
            "चिटेगाव",
            "चिटे पिंपळगाव",
            "चिंचोली",
            "जळगाव फेरन",
            "जाकमाथा",
            "जोगवाडा",
            "झळता",
            "डोनवाडा",
            "तिसगाव",
            "तोनगाव",
            "तोहळ नाईक तांडा",
            "दुधाड",
            "देमानी",
            "दौलताबाद",
            "धरमपूर",
            "धोंडखेडा",
            "निपानि",
            "नैगव्हाण",
            "पाचोड",
            "पारदरी",
            "पारदरी तांडा",
            "पिंपरी खु.",
            "बलापूर",
            "बागतळब",
            "बोरवाडी",
            "बोरवाडी तांडा",
            "भिनडोन",
            "माळीवाडी",
            "रुस्तुमपूर",
            "लिंगदरी",
            "विठ्ठलपूर",
            "वंजारवाडी",
            "शिवगड तांडा",
            "शेकापूर",
            "शेक्ता",
            "शेर्नापूर",
            "शेवगा",
            "सिंडोन",
            "हतमाळी",
            "अळापूर",
            "अंतापूर",
            "अंबेगाव खु.",
            "ऊंबरखेडा",
            "ऊंबरखेडा तांडा",
            "औरळा",
            "औरळी",
            "औरंगपूर",
            "कळंकी",
            "कानडगाव (कन्नड)",
            "कारंजखेडा खालसा",
            "कारंजखेडा जहागिर",
            "कुंजखेडा",
            "केसापूर",
            "कोळवाडी",
            "खडकी",
            "खाटखेडा",
            "खापरखेडा",
            "खामगाव",
            "खोलापूर",
            "चांभारवाडी",
            "चिमनापूर",
            "जामडी (घाट)",
            "तापरगाव",
            "ताळनेर",
            "दाभाडी",
            "दिगर",
            "दुधमाळ",
            "देभेगाव",
            "धामणी खु.",
            "धार्णखेडा",
            "नागापूर",
            "पळसी खु.",
            "पळसी बु.",
            "पिशोर",
            "बोरसार खुर्द",
            "माटेगाव",
            "मालपूर",
            "माळेगाव (ठोकळ)",
            "माळेगाव (डी)",
            "मुंडवाडी",
            "मुंडवाडी तांडा (नविन)",
            "मेहूण पुरणवाडी",
            "मोहर्डा",
            "रीक्त",
            "रीठी",
            "रुईखेडा",
            "रेवूळगाव",
            "रोहीला खु.",
            "लव्हाळी",
            "लामणगाव",
            "वडनेर",
            "वडनेर तांडा",
            "वाडीचिमनापूर",
            "वैसपूर",
            "शफेपूर",
            "सहानगाव",
            "साखरवेल",
            "सावरगाव",
            "सितानाईक तांडा (नविन)",
            "अझमपूर",
            "इंदापूर",
            "कांकशिल",
            "खासपूर",
            "गल्लेबोरगाव",
            "चिंचोली",
            "ताजनापूर",
            "दरेगाव",
            "धामणगाव",
            "निरगुडी खु.",
            "निरगुडी बु.",
            "पळसगाव",
            "पाडळी",
            "पिंपरी",
            "बाजार सावंगी",
            "मावसळा",
            "रैळ",
            "वेरुळ",
            "शेखापूर",
            "सुलतानबाद",
            "सोबळगाव",
            "अगर कानडगाव",
            "अलमगिरपूर",
            "असेगाव",
            "अंबेगाव",
            "अंबेगाव बु.",
            "एकलहरा",
            "कासोडा",
            "कोळघर",
            "खैरगव्हाण",
            "डोनगाव",
            "ताळेसामन",
            "तांदुळवाडी",
            "देवळी",
            "दैगाव",
            "धामोरी खु.",
            "नांदेडा",
            "नेवेरगाव",
            "पाचपिरवाडी",
            "फाजलपूर",
            "बागडी",
            "बाबरगाव",
            "बाळापूर",
            "बुट्टे वडगाव",
            "बोलेगाव",
            "बोळठाण",
            "ममदापूर",
            "मळुंजा खु.",
            "महमदपूर",
            "महोळी",
            "मांजरी",
            "मुधेशवडगाव",
            "मुस्तफाबाद",
            "मौजुदाबाद",
            "मंजारपूर",
            "वरखेड",
            "वाजनापूर",
            "वाहेगाव",
            "शेक्ता",
            "सरीफपूर",
            "सावंगी",
            "सांजरापूर",
            "सांजराबाद",
            "सिद्धनाथ वडगाव",
            "सिरसगाव",
            "सिरेगाव",
            "सिल्लेगाव",
            "सुरेवाडी",
            "सुलतानाबाद",
            "हकिकतपूर",
            "हदीयाबाद",
            "अगापूर",
            "अपेगाव",
            "आनंदपूर",
            "ईनायतपूर",
            "ईंदेवोन",
            "काडेठाण खु.",
            "काडेठाण बु.",
            "कातपूर",
            "कासारपाडळी",
            "कुतूब खेडा",
            "कौंदर",
            "खाडगाव",
            "खेर्डा",
            "गेवराई मर्डा",
            "तांडा खु.",
            "तांडा बु.",
            "तुपेवाडी",
            "थेरगाव",
            "दरेगाव",
            "दाडेगाव खु.",
            "दाडेगाव बु.",
            "दावरवाडी",
            "दियंतपूर",
            "देरा",
            "नईगाव",
            "नानेगाव",
            "नायगाव",
            "नारायणगाव",
            "नांदर",
            "पाचळगाव",
            "पारुंडी",
            "पारुंडी तांडा",
            "पिसेगाव",
            "बाळानगर",
            "ब्रम्हागाव",
            "मायगाव",
            "मुधाळवाडी",
            "यासिनपूर",
            "रांजणगाव दंडगा",
            "रीक्त",
            "लिंबगाव",
            "वडजी",
            "वडाळा",
            "वरुडी बु.",
            "ववा",
            "वहेगाव",
            "वाघाडी",
            "शहापूर वहेगाव",
            "श्रिंगारवाडी",
            "साळवडगाव",
            "सुलतानपूर",
            "सोनवाडी खु.",
            "सोनवाडी बु.",
            "सोळनपूर",
            "हर्शी खु.",
            "हर्शी बु.",
            "हिंगणी",
            "कविटखेडा",
            "कान्हेगाव",
            "खामगाव",
            "चिंचोली (नाकीब)",
            "जानेफळ",
            "नायगाव",
            "निढाणा",
            "पाडळी ववना",
            "पानवाडी",
            "पिंपळगावदेव",
            "पोखरी",
            "फुलंब्री",
            "बाब्रा",
            "बाभुळगाव खु.",
            "बिळदा",
            "मरसावळी",
            "महाळ किन्होळा",
            "म्हासळा",
            "रेलगाव",
            "लाळवण",
            "लेहा बाब्रा",
            "वरेगाव",
            "वहेगाव",
            "वाकोड",
            "वाणेगाव खु.",
            "वावना",
            "शहापूर",
            "सोनरि बु.",
            "सोनरी खु.",
            "अमानतपूरवाडी",
            "अंचळगाव",
            "ईकोडी सागज",
            "उंदीरवाडी",
            "औरंगपूर",
            "कानक सागज",
            "कारंजगाव",
            "खिरीडी",
            "खंडाळा",
            "गारज",
            "गोलवाडी",
            "चिंचदगाव",
            "चोरवाघळगाव",
            "झोळेगाव",
            "टाकळी सागज",
            "डोनगाव",
            "तरट्याचीवाडी (नविन)",
            "तुंकी",
            "दहेगाव",
            "दावळा",
            "देवगाव शानी",
            "धोंडळगाव",
            "नाळेगाव",
            "नैगव्हाण",
            "पाथरी",
            "पानगव्हाण",
            "पान्वी बु.",
            "पाळखेड",
            "पाशपूर",
            "पोखरी",
            "बल्लाळी सागज",
            "बाभुळगाव खु.",
            "बाभुळगाव बु.",
            "बाभुळतेल",
            "बेळगाव",
            "बोरसर",
            "भवूर",
            "भागूर",
            "भिवगाव",
            "भिंगी",
            "भैगाव गंगा",
            "महालगाव",
            "माळी घोगरगाव",
            "माळी सागज",
            "मांडकी",
            "रघुनाथपूरवाडी",
            "राहेगव्हाण",
            "राहेगाव",
            "रीक्त",
            "लखमापूरवाडी",
            "लाखगंगा",
            "वघाळा",
            "वाकटी",
            "वाकळा",
            "शिवगाव",
            "शिवूर",
            "सावखेड खंडाळा",
            "सुराळा",
            "सोनवाडी",
            "संजारपूरवाडी",
            "हजीपूरवाडी",
            "हनुमंतगाव",
            "हमरापूर",
            "हरगोविंदपूर",
            "हिंगोणी",
            "अजंठा",
            "अन्वी",
            "असडी",
            "अंधारी (सिल्लोड)",
            "उंडवोनगाव",
            "काजीपूर",
            "कासोड",
            "कुतूबपूर",
            "खुल्लोड",
            "खंडाळा",
            "गोलेगाव खु.",
            "गोलेगाव बु.",
            "चंदापूर",
            "जळकी घाट",
            "जळकी (वसई)",
            "दाकळा",
            "दिग्रस",
            "धानोरा",
            "धामनी",
            "धोत्रा",
            "पानस",
            "पाळोद",
            "पिरोळा",
            "पिंपरी",
            "पिंपळगाव पेठ",
            "पिंपळदरी",
            "बळापूर",
            "बोदवड",
            "भरडी",
            "भावन",
            "मुखपथ",
            "रहिमबाद",
            "रांजनी",
            "लेहा",
            "वडादपान खु.",
            "वडोद चाथा",
            "वडोदपान बु.",
            "वरुड खु.",
            "वसई",
            "वांगी खु.",
            "वांगी बु.",
            "वांजोळा",
            "सराटी",
            "सारोळा",
            "हळदा",
            "अनद",
            "गळवाडा",
            "गोंडेगाव",
            "घोरकुंड",
            "जंगली कोठा",
            "तिटवी",
            "तितूर",
            "दाभा",
            "दाव्हरी",
            "नांदा",
            "निमखेडी",
            "निंबायती",
            "पळशी",
            "पळसखेडा (सावळा)",
            "पिंपळवाडी",
            "बनोती",
            "मुखेड",
            "रीक्त",
            "लेनापूर",
            "वरठाण",
            "वाडी सुतोंडा",
            "सावरखेडा",
            "सावळदबारा",
            "हनुमानखेडा",
            "अटाळे",
            "अनोरा",
            "अंबासन",
            "आर्डी",
            "कान्हेरे",
            "खडके",
            "खोकर पाट",
            "गलवाडे खु.",
            "चाकवे",
            "चिमनपूरी",
            "जानवे",
            "झाडी",
            "डांगर",
            "ढेकूचारम",
            "ढेकू सीम",
            "तळवाडे",
            "दापोरी बु.",
            "निसर्डी",
            "पिंपळे खु.",
            "पिंपळे बु.",
            "फापोरे खु.",
            "बहादरवाडी",
            "बिलखेडे",
            "रडावण",
            "रणाईचे बु.",
            "लोणे",
            "लोंढवे",
            "वाघोडे",
            "शिरसाळे खु.",
            "शिरसाळे बु.",
            "सडावण खु.",
            "सडावण बु.",
            "सुंदरपट्टी",
            "सोनखेडी",
            "अंतुर्ली खु.",
            "आडगाव",
            "आनंदनगर",
            "आंबे",
            "उत्राण(अ.ह.)",
            "उत्राण(गु.ह.)",
            "उमरे",
            "खर्ची खु.",
            "जवखेडे सिम",
            "जानफळ",
            "तळई",
            "ताडे",
            "नागदुली",
            "निपाणे",
            "पिंप्री सिम",
            "बाम्हणे",
            "भातखेडे",
            "मालखेडे बु.",
            "वाघळूद सिम",
            "हनुमंतखेडे सिम",
            "अभोणे",
            "आडगाव",
            "आंबेहोळ",
            "उपखेड",
            "ऊंबरखेड",
            "ओझर",
            "कढरे",
            "करंजगाव",
            "कळमडू",
            "कृष्णानगर (नविन)",
            "कृष्णापूरी",
            "खडकी बु.",
            "खडकी सीम",
            "खरजई",
            "खेडगाव",
            "खेडी खु.",
            "खेर्डे",
            "चिंचखेडे",
            "चिंचगव्हाण",
            "जामडी प्र.बहाळ",
            "डोणदिगर",
            "तळेगाव",
            "तळोदे प्र.चाळीसगाव",
            "तिरपोळे",
            "दरेगाव",
            "दसेगाव बु.",
            "दहीवड",
            "देवळी",
            "परशरामनगर",
            "पळासरे",
            "पिंपरी खु.",
            "पोहरे",
            "बहाळ",
            "बाणगाव",
            "बोढरे",
            "मुंदखेडा बु.",
            "राजमाने",
            "रामनगर",
            "रांजणगाव",
            "रोकडे",
            "लोंजे",
            "लोंढे",
            "वरखेड बु.",
            "वाकडी",
            "वाघरी",
            "विसापूर",
            "सांगवी",
            "सुंदरनगर",
            "सोनगाव (नविन)",
            "हिरापूर",
            "अंबाडे",
            "कठोरा",
            "कर्जाने",
            "कोळंबे",
            "खरग",
            "खाचणे",
            "खा-यापाडा",
            "गोरगावले बु.",
            "घुमावल खु.",
            "तावसे खु.",
            "तांदळवाडी",
            "देवझरी",
            "देव्हारी",
            "नारवाडे",
            "नारोड",
            "निमगव्हाण",
            "बोरअजंटी",
            "बोरखेडे",
            "बोरमळी",
            "मचाळे",
            "माळापूर",
            "मुळयाउतार",
            "मेलाणे",
            "ऱुखनखेडे प्र. चो.",
            "वडगाव खु.",
            "वडगाव सीम",
            "वडती",
            "विरवाडे",
            "विष्णापूर",
            "वेजापूर (शेणपाणी) ",
            "वैजापूर",
            "आसोदा",
            "कानसवाडे",
            "कुसुंबे खु.",
            "कंडारी",
            "जळके",
            "तरसोद",
            "धामणगाव",
            "निमगाव बु.",
            "बिलखेडे",
            "बिलवाडी",
            "बेळी",
            "भागपूर",
            "भादली बु.",
            "भोलाणे",
            "मन्यार खेडे",
            "ममुराबाद",
            "रामदेववाडी",
            "रायपूर",
            "वडनगरी",
            "वराड खु.",
            "वराड बु.",
            "वसंतवाडी",
            "वावडदा",
            "विटनेर",
            "शेळगाव",
            "सुभाषवाडी",
            "अंबाडी",
            "अंबिलहोळ",
            "अंबिलहोळ देवीचे",
            "करमाड",
            "कापूसवाडी",
            "कालखेडा",
            "कुंभारी खु.",
            "कुंभारी बु.",
            "कुंभारी सिम",
            "खादगाव",
            "खांदवे",
            "गाडेगाव प्र. न.",
            "गोद्री",
            "गोरनाळे",
            "चिंच खेडे दिगर",
            "जंगीपुरा",
            "डोहरी",
            "तिघ्रे वडगाव",
            "तोंडापूर",
            "देऊळगाव",
            "देवळसगाव",
            "नांदरे हवेली",
            "नांद्रा प्र. लो.",
            "नेरी दिगर",
            "पळासखेडे काकर",
            "पळासखेडे मिराचे",
            "पाट खेडे",
            "बेटावद खु.",
            "बेटावद बु.",
            "भारुड खेडे",
            "मालखेडे",
            "मालदाभाडी",
            "मांडवे खु.",
            "मांडवे बु.",
            "मोयखेडा दिगर",
            "मोरगाव  .",
            "मोहाडी",
            "रामपूर",
            "रामपूर न.व.",
            "रांजनी",
            "रोटवद",
            "वडगाव तिघरे",
            "वडाळी",
            "वाघारी",
            "वाडी",
            "शेंदूर्णी",
            "सारगाव",
            "सार्वे प्र. लोहारे",
            "सावत खेडे",
            "हरीनगर",
            "हिवर खेडे तर्फे वाकडी",
            "हिवर खेडे बु.",
            "हिंगणे बु.",
            "अनोरे",
            "उखळवाडी",
            "कंडारी बु.",
            "गारखेडे",
            "गोंदेगाव",
            "गंगापूरी बु.",
            "जांभोरे",
            "तरडे खु.",
            "धरणगाव (रुरल)",
            "धानोरे",
            "पष्टाणे खु.",
            "पष्टाणे बु.",
            "बाभळे बु.",
            "बिलखेडे",
            "भामर्डी",
            "भोने बु.",
            "महांकाळे (उजाड)",
            "वाघळूद खु.",
            "शामखेडे",
            "साकरे",
            "सार्वे खु.",
            "हनमंतखेडे खु.",
            "अंतुर्ली खु.प्र.पाचोरा",
            "अंतुर्ली खु.प्र.लोहारे",
            "अंतुर्ली बु.प्र.पाचोरा",
            "ओझर",
            "कळमसरे",
            "कासमपुरे",
            "खेडगाव",
            "गाळण बु.",
            "चिंचखेडे बु.",
            "तारखेडे खु.",
            "तारखेडे बु.",
            "दिघी",
            "दुसखेडे",
            "नाईकनगर",
            "पहाण",
            "पुनगाव",
            "बांबरूड खु.प्र.पा.",
            "भडळी",
            "भातखंडे खु.",
            "मांडकी",
            "मोहाडी",
            "म्हसास",
            "रामेश्वर",
            "लाख",
            "लोहारा",
            "वडगाव खु.प्र.पा.",
            "वेरुळी बु.",
            "शहापूरे",
            "सार्वे खु.प्र.भ.",
            "हडसन",
            "हनुमानवाडी",
            "करंजी बु.",
            "कोळपिंपरी",
            "कंकराज",
            "चहूत्रे",
            "चोरवड",
            "टिटवी",
            "टिटवी सीम",
            "दगडी प्र. अ.",
            "दबापिंप्री",
            "दळवेल",
            "धुळपिंप्री",
            "नेरपाट",
            "पारोळा रुरल",
            "पिंपळ भैरव",
            "पिंप्री प्र. उ.",
            "बोदर्डे",
            "भिलाली",
            "भोकरबारी",
            "भोंडणदिगर",
            "मुंदाणे प्र. अ.",
            "मोंढाळे प्र. अ.",
            "मंगरुळ",
            "रत्नापिंप्री",
            "राजवड",
            "लोणी खु.",
            "वडगाव प्र. एरंडोल",
            "वसंत वाडी",
            "वंजारी खु.",
            "शेवगे प्र. ब.",
            "शेवगे बु.",
            "सब गव्हाण प्र. अमळनेर",
            "सुब गव्हाण खु.",
            "सोके",
            "हिवरखेडे खु.",
            "हिवरखेडे बु.",
            "हिवरखेडे सीम",
            "होळपिंप्री",
            "जलचक्र खु.",
            "जलचक्र बु.",
            "जामठी",
            "धानोरी",
            "पळासखेडे बु.",
            "बोरगाव",
            "मुक्तळ",
            "येवती",
            "रेवती",
            "लोनवाडी प्र. बोदवड",
            "वराड खु.",
            "वराड बु.",
            "वाकी",
            "शेलवड",
            "अडळसे",
            "अंतुर्ली बु.",
            "आंचळगाव",
            "कोळगाव",
            "खेडगाव खु.",
            "तळवण तांडा",
            "धोत्रे",
            "पथराड",
            "पिचर्डे",
            "पिंपरखेडे",
            "पिंप्रीहाट",
            "पेंडगाव",
            "बात्सर",
            "भातखंडे बु.",
            "वसंतवाडी",
            "शिंदी",
            "सावदे",
            "अंजनसोंडे",
            "खडके",
            "तळवेल",
            "दर्यापूर",
            "पिंपळगाव खु.",
            "फुलगाव",
            "फेकरी",
            "बेलखेडे",
            "बोहर्डी खु.",
            "बोहर्डी बु.",
            "भुसावळ (रुरल)",
            "वझरखेडे",
            "साकरी",
            "सुसरी",
            "अंतुर्ली",
            "ईच्छापूर",
            "उचंदे",
            "उमरे",
            "ओगर",
            "कु-हे",
            "कुंड",
            "कोथळी",
            "को-हाळे",
            "खामखेडे",
            "घोडसगाव",
            "चांगदेव",
            "चिखली",
            "चिंचखेडे खु.",
            "चिंचखेडे बु.",
            "चिंचोल",
            "जोंधन खेडे",
            "टाकळी",
            "डोलारखेडा",
            "ढोरमाळ",
            "तरोडा",
            "ताळखेडे",
            "थेरोळे",
            "धामंदे",
            "धुळे",
            "नरवेल",
            "नायगाव",
            "नांदवेल",
            "निमखेडी बु.",
            "पातोंडी",
            "पारंबी",
            "पिंप्राळे",
            "पिंप्री नांदू",
            "पिंप्री पंचम",
            "पुरनाड",
            "पंचाणे",
            "बहादरपूर (उजाड)",
            "बेलखेडे",
            "बेलसवाडी",
            "भांडगुरे",
            "भोकरी",
            "भोटा",
            "मन्यारखेडे",
            "महालखेडा",
            "मानेगाव",
            "मुक्ताईनगर",
            "मुढळदे",
            "मेळ सांगवे",
            "मेहूण",
            "मेंढोळदे",
            "रीगाव",
            "रुईखेडे",
            "वडॉदा",
            "वायला",
            "शेमळदे",
            "सारोळे",
            "सालबर्डी",
            "सुकळी",
            "सुळे",
            "हरताळा",
            "हलखेडे",
            "हिवरे",
            "अकलूद",
            "अमोदे",
            "अंजाळे",
            "आंबापाणी",
            "कठोरे प्र. सा.",
            "कोरपावली",
            "थोरगव्हाण",
            "दगडी",
            "दहीगाव",
            "दुसखेडे",
            "नावरे",
            "पथराळे",
            "पाडळसे",
            "पिळोदे खु.",
            "बोरखेडे बु.",
            "बोराळे",
            "मनवेल",
            "महेलखेडी",
            "मारुळ",
            "मोहराळे",
            "वढोदे प्र.या.",
            "वाघझिरा",
            "विरावली",
            "शिरसाड",
            "शिरागड",
            "साकळी",
            "हरीपूरा",
            "अभोडे खु.",
            "अंधारमळी",
            "उदळी खु.",
            "के-हाळे खु.",
            "गहूखेडे",
            "गारखेडे",
            "गुलाबवाडी",
            "चोरवड",
            "जिन्सी",
            "जुनोने",
            "तासखेडे",
            "तिड्या",
            "निमडया",
            "पाडळे खु.",
            "पाल",
            "पिमपरकुंड",
            "भोर",
            "मोरवहळ",
            "मोहमांडली (नविन)",
            "रणगाव",
            "रायपूर",
            "सुदगाव",
            "ईश्वरनगर",
            "कातखेडा",
            "कुकडगाव",
            "कौडगाव",
            "खडकेश्वर",
            "गोला",
            "गोविंदपूर",
            "गंगारामवाडी",
            "चिंचखेड",
            "झिर्पी",
            "झिर्पी तांडा",
            "ताधडगाव",
            "दधेगाव",
            "दावरगाव",
            "दुधपूरी",
            "धनगर पिंपरी",
            "धाळसखेडा",
            "नारायणगाव",
            "पारडा",
            "पारनेर",
            "पावसे पांगरी",
            "पांगरखेडा",
            "पिठोरी सिरसगाव",
            "बनटाकळी",
            "बेळगाव",
            "बोरी",
            "भाटखेडा",
            "भाथन खु.",
            "भाळगाव",
            "भिवंडी बोडखा",
            "मढ तांडा",
            "मर्डी",
            "माठ जळगाव",
            "मुसई",
            "रामनगर",
            "राहूवाडी (सेवलालनगर)",
            "रेवळगाव",
            "लाखमपूरी",
            "लालवाडी",
            "वळखेडा",
            "वसंतनगर",
            "वागळखेडा",
            "वाडीकळ्या",
            "वाडी लासूरा",
            "वाडी शिरढोण",
            "शहापूर",
            "शिरढोण",
            "शेवगा",
            "सारंगपूर",
            "सिरनेर",
            "सुखापूरी",
            "सोनक पिंपळगाव",
            "हरत खेडा",
            "हस्त पोखरी",
            "आंतरवळी टेंभी",
            "आंतरवळी राठी",
            "एकरुखा",
            "कांदरी अंबड",
            "कृष्णानगर",
            "कोठी",
            "खडकवाडी",
            "खडका",
            "खापरदेवहिवरा",
            "खालापूरी",
            "घनसावंगी",
            "ताळेगाव",
            "दहिगव्हाण बु.",
            "देवनगर",
            "देवहिवरा",
            "देवोळी अंबड",
            "देवोळी परतूर",
            "दैठाणा बु.",
            "धाकेफळ",
            "पराडगाव",
            "पाडूळी बु.",
            "पांगरा",
            "पांगरा तांडी",
            "पिरगैबवाडी",
            "बहिरेगाव",
            "बाचेगाव",
            "बोडखा बु.",
            "बोर रांजनी",
            "बोळेगाव",
            "बोंधळापूरी",
            "भाईगव्हाण",
            "भादरेगाव",
            "भोगगाव",
            "मच्चिद्रनाथ चिंचोली",
            "मुढेगाव",
            "मुद्रेगाव",
            "मोहपूरी",
            "मंगरुळ",
            "मंडळ",
            "यावल पिंपरी",
            "यावल पिंपरी तांडा",
            "राजेगाव",
            "राणी उंचेगाव",
            "रामगव्हाण खु.",
            "राहेरा",
            "रांजनी",
            "रीक्त",
            "वाडी रामसगाव",
            "अकोला देव",
            "आंबेगाव",
            "कोनड",
            "कोळेगाव",
            "गणेशपूर",
            "गाडेगव्हाण",
            "चापनेरा",
            "जवखेडा ठेंग",
            "टेंभुर्णी",
            "डोनगाव",
            "तपोवन गोंधन",
            "दहिगाव",
            "दावरगाव",
            "देवळे गव्हाण",
            "धोंडखेडा",
            "निमगाव खु.",
            "निवडुंग",
            "पिंपळगाव काड",
            "पोखरी",
            "बुतखेडा",
            "बेळोरा",
            "ब्राम्हणपूरी",
            "भरडखेड",
            "रीक्त",
            "वरखेडा विरो",
            "वरुड बु.",
            "सातेफळ",
            "सावरखेडी गोंधन",
            "सावंगी",
            "सांजोळ",
            "सिपोरा अंभोरा",
            "सोनखेडा",
            "हनुमंत खेडा",
            "अहंकार देवळगाव",
            "आंबेडकरवाडी",
            "उम्री",
            "एरंडवडगाव",
            "काडवांची",
            "कुंभेफळ शिंदखेड",
            "कोळवाडी",
            "खराती",
            "गवळी पोखरी",
            "गोकुळनगर",
            "गोलपांग्री",
            "गोलावाडी",
            "गोंडेगाव",
            "घानेवाडी",
            "जळगाव",
            "ताटेवाडी",
            "थर",
            "थेरगाव",
            "दुधाना काळेगाव",
            "धारकल्याण",
            "धारा",
            "नाव्हा",
            "नांदापूर",
            "निढोणा",
            "निरखेडा",
            "पत्रा तांडा",
            "पळसखेडा",
            "पाचनवडगाव",
            "पिरपिंपळगाव",
            "पिंपळवाडी",
            "पीरकल्याण",
            "पोखरी शिंदखेड",
            "बाजी उम्रड",
            "बाजी उम्रड तांडा",
            "बाथन बु.",
            "बिबी",
            "बोरखेडी",
            "बोरगाव",
            "ब्राम्हणखेडा",
            "भाटखेडा",
            "माळशेंद्रा",
            "माळेगाव खु.",
            "राईगव्हाण",
            "रोहनवाडी",
            "लोंढ्याचीवाडी",
            "वडगाव",
            "वरखेडा(नेर)",
            "वरखेडा(सिंधखेड)",
            "वरुड",
            "वाखरी",
            "वाघरुळ (जहागीर)",
            "वाडीवाडी",
            "वंजार उम्रड",
            "शिवनी",
            "सारवाडी (जालना)",
            "सावरगाव भागडे",
            "सेवळी",
            "सोनदेव",
            "सोमनाथ",
            "अकोली",
            "अष्टी",
            "असनगाव",
            "अंतरवाळा",
            "अंबा",
            "आनंदगाव",
            "आनंदवाडी",
            "क-हाळा",
            "कावजावळा",
            "कोकाटे हदगाव",
            "चांगतपूरी",
            "ढोणवाडी",
            "तोरणा",
            "देवळा",
            "दोल्हारा",
            "परातवाडी",
            "पांडेपोखरी",
            "पिंपळी धामणगाव",
            "बनाचीवाडी",
            "मासळा",
            "येनोरा",
            "रीक्त",
            "लिंगसा",
            "लोणी खु.",
            "वडारवाडी",
            "वढोणा",
            "वहेगाव शिरष्टी",
            "वहेगाव सातारा",
            "शिरष्टी तांडा",
            "शेलगाव",
            "शेवगा",
            "सातारा",
            "सातोना खु.",
            "सातोना बु.",
            "सांकनपूरी",
            "सिरसगाव",
            "सुरुमगाव",
            "सोईजना",
            "हतडी",
            "हनवाडी",
            "असोळा",
            "आसरखेडा",
            "काजळा",
            "किन्होळा",
            "घोतन",
            "चानेगाव",
            "चितोडा",
            "डावरगाव",
            "डोंगरगाव सायगाव",
            "तुपेवाडी",
            "दगडवाडी",
            "दाभाडी",
            "धामणगाव",
            "पाथर देवोळगाव",
            "पानखेडा",
            "बाजार वहेगाव",
            "बावने पांगरी",
            "बुटेगाव",
            "भाटखेडा",
            "मानदेवोळगाव",
            "मालेगाव",
            "मांडवा",
            "मेव्हना",
            "म्हासळा",
            "रांजणगाव",
            "रीक्त",
            "विळहाडी",
            "सिंधी पिंपळगाव",
            "हिवरा दाभाडी",
            "ईकेफळ",
            "ईब्राहिमपूर ",
            "उमरखेडा",
            "केदारखेडा",
            "कोडोळी",
            "कोसगाव",
            "खामखेडा",
            "गारखेडा",
            "चांदई ईको",
            "चांदई टेपळी",
            "चांदई थोंबरी",
            "जवखेडा बु.",
            "जैदेववाडी",
            "जोमळा",
            "टाकळी भोकरदन",
            "तपोवन",
            "नसिराबाद",
            "पद्मावती",
            "पळसखेडा थोंबरी",
            "पळसखेडा दाभाडी",
            "पळसखेडा पिंपाळे",
            "पिंपळगाव थोट",
            "पिंपळगाव बाराव",
            "पिंपळगाव (रेणूकाई)",
            "पेरजापूर ",
            "पंढरपूर (नविन)",
            "फत्तेपूर ",
            "बानेगाव",
            "बामखेडा",
            "मेहगाव",
            "मोहळाई",
            "राजळा",
            "राजापूर",
            "राजूर",
            "रीक्त",
            "रेलगाव",
            "लिंगेवाडी",
            "लोनगाव",
            "वढोणा",
            "वरुड बु.",
            "वळसखालसा",
            "वळसावंगी",
            "विझोरा",
            "शिरसगाव (मंडप)",
            "सावंगी औघडराव",
            "सुभानपूर ",
            "सुंदेरवाडी",
            "अंधवाडी",
            "अंभोडा कदम",
            "अंभोर शेळके",
            "अंभोरा जहागिर",
            "उसवड (देवथाणा)",
            "किनखेडा",
            "किर्तापूर",
            "किर्तापूर तांडा",
            "किर्ला",
            "केहळवडगाव",
            "केंधळज",
            "खोरड सावंगी",
            "खोरवड",
            "गणेशपूर ",
            "गारटेकी",
            "गुळखांड",
            "चिखली",
            "जैपूर ",
            "ताळेगाव",
            "दहीफळ खांदरे",
            "दुधा",
            "देवगाव खवटे",
            "नायगाव प.सेवली",
            "नैगाव प.बाम्हणी",
            "पाडळीदुधा",
            "पांगरा गदाधे",
            "पांगरी (गोसावी)",
            "पिंपारखेडा खारबे",
            "पेवा",
            "पोखरी टाकळे",
            "बेळोरा",
            "भुवन",
            "महोरा",
            "माळसावंगी",
            "मोहदरी",
            "मंगरुळ",
            "लावणी",
            "लिंबखेडा",
            "वझार सारकाटे",
            "वढेगाव (पांधुर्ना)",
            "वरुड",
            "वैध्यवडगाव",
            "शिवंगिरी",
            "सासखेडा",
            "सोनूंकरवाडी",
            "हनवत खेडा",
            "कामठा बु.",
            "कोंढा",
            "खडकी",
            "गणपूर",
            "निजामपूर",
            "मेंधळा बु.",
            "शेलगाव बु.",
            "सावरगाव",
            "गणीपूर",
            "जिरोना",
            "तुरटी",
            "धानोरा बु.",
            "बितनळ",
            "बोथी",
            "बोळसा खु.",
            "मोखंडी (जागिर)",
            "मंडाळा",
            "रामखडक",
            "वाघळा",
            "सावरगाव (काळा)",
            "सोमठाणा पी.यू",
            "हिरडगाव",
            "हुंडा तांडा",
            "हुंडा पत्ती उमरी",
            "अमाडी",
            "अंजी",
            "इरेगाव",
            "इसळापूर",
            "उनकदेव",
            "कारंजी",
            "कुपटी खु.",
            "कुपटी बु.",
            "कोठारी (चिखली)",
            "कोल्लारी",
            "कोसमेत",
            "चिखली (आय.)",
            "चिखली तांडा",
            "चिखली बु.",
            "जरोळा तांडा",
            "झलकवाडी",
            "टिटवी",
            "तल्लारी",
            "तल्लारी तांडा",
            "थारा",
            "दाभाडी",
            "दारसांगवी (चिखली)",
            "दारसांगवी (सिंधखेड)",
            "नवरगाव",
            "नाखातेवाडी",
            "नांदगाव",
            "नांदगाव तांडा",
            "पार्डी",
            "पार्डी खु.",
            "पार्डी बु",
            "पांगरी तांडा",
            "पांढरा",
            "पांर्गी",
            "पिंपारफोडी",
            "प्रधान सांगवी",
            "बुधवार पेठ",
            "बुर्कूळवाडी",
            "बेनडी",
            "बेनडी तांडा",
            "बेल्लोरी (किनवट)",
            "बोथ",
            "बोधडी खु.",
            "बोधाडी बु.",
            "भिसी",
            "मदनापूर (चिखली)",
            "मळकवाडी",
            "माळबोरगाव",
            "मुलझरा",
            "मोहपूर",
            "मोहाडा तांडा",
            "रीठा तांडा",
            "रोडा नाईक तांडा",
            "लिंगधारी (डी)",
            "लोखंडवाडी",
            "वळकी खु.",
            "शनिवारपेठ",
            "साकळू नाईक तांडा",
            "सावरी",
            "सांगवी",
            "सिंदगी (मो)",
            "सोनपेठ",
            "सोनवाडी",
            "हतोळा",
            "हुडी (इसळापूर)",
            "हुडी (डी )",
            "अळेगाव",
            "औरळ",
            "कल्लळी",
            "गोगदरी",
            "गोनर",
            "गौंडा",
            "चिखली",
            "चिंचोळी पी.के",
            "चौकी धरणापूरी",
            "चौकी (महाकाया)",
            "जाकापूर",
            "दहीकळंबा",
            "दाताळा",
            "दिंडा",
            "देवाचीवाडी",
            "नंदनवन",
            "पेठवडज",
            "बाचोती",
            "बामनी पी.के",
            "बिंदा",
            "माजरे वरवट",
            "मंगल सांगवी",
            "येलूर",
            "रुई",
            "शिर्सी खु.",
            "शिर्सी बु.",
            "सावरगाव निपानि",
            "सावळेश्वर",
            "हळदा",
            "हिस्से औरळ",
            "अपसावरगाव",
            "अळापूर",
            "ईब्राहिमपूर",
            "काथेवाडी",
            "कारेगाव",
            "कुतूब शहापूरवाडी",
            "चाकूर",
            "झरी",
            "टाकळी जहागिर",
            "देगाव खु.",
            "देवापूर",
            "नागरळ",
            "निपानि सावरगाव",
            "पिंपळगाव",
            "पेंडपल्ली",
            "बाल्लूर",
            "भक्तापूर",
            "माळेगाव (एम)",
            "मैलापूर (डी)",
            "मंशाकार्गा",
            "येरगी",
            "रामपूर बु.",
            "लख्खा",
            "लिंगनकेरुर",
            "शिवनी",
            "सुगाव",
            "सुंडगी खु.",
            "होतळ",
            "चेनापूर (डी)",
            "पातोडा थाडी",
            "पांग्री",
            "बाचेगाव",
            "बामणी",
            "मानूर",
            "माष्टी",
            "मोकाळी",
            "राजापूर",
            "विळेगाव तांडी (बु)",
            "शेलगाव थाडी",
            "संगम",
            "अळूवडगाव",
            "कार्ला टी.एम.",
            "कांदळा",
            "कुंचोळी",
            "केदार वडगाव",
            "कोप्रा",
            "खांडगाव",
            "गाडगा",
            "गोलेगाव",
            "टाकळी (टी.एम.)",
            "टेंभूर्णी",
            "धानोरा टी.एम.",
            "धुप्पा",
            "नावंडी",
            "बेंद्री",
            "महेगाव",
            "मारवळी",
            "मारवळी तांडा",
            "मुगाव",
            "मुस्तापूर",
            "मोकासदरा",
            "मंजीराम",
            "मंजीरामवाडी",
            "रातोळी",
            "शेळगांव गौरी",
            "कोटतिर्थ",
            "गंगाबेट",
            "जैतापूर",
            "ढोकी",
            "थुगाव",
            "दर्यापूर",
            "धानोरा",
            "नळेश्वर",
            "पिंपळगाव कोरका",
            "पोखरनी",
            "बोरगाव तेळंग",
            "भानपूर",
            "राहटी बु.",
            "लिंबगाव",
            "वनेगाव",
            "वरखेड",
            "वाघी",
            "सायळ",
            "सुगाव खु.",
            "सुगाव बु.",
            "सोमेश्वर",
            "हस्सापूर",
            "अजिजाबाद (डी)",
            "अरळी",
            "का-हळ",
            "किनाळा",
            "कोल्हेबोरगाव",
            "गागळेगाव",
            "जिगळा",
            "टाकळी थाडी",
            "तळनी",
            "पिंपळगाव (के)",
            "पंचिमपळी",
            "बेळकोनी खु.",
            "बेळकोनी बु.",
            "रामतिर्थ",
            "लोहगाव",
            "हाज्जापूर",
            "हिप्पर्गा (मा)",
            "खडकी",
            "गारगोटवाडी पा.",
            "जामदरी",
            "जामदरी तांडा",
            "जांभळी",
            "डोर्ली",
            "नांदा पट्टी म्हैसा",
            "पिंपळधव",
            "पंडूर्ना",
            "बल्लाळ",
            "बेंद्री",
            "बोरवाडी",
            "मातूळ",
            "रावणगाव",
            "राहटी खु. (साजा)",
            "लागळूद",
            "समंदरवाडी",
            "खुपती (नाहूर)",
            "दहेगाव (सा)  (नविन)",
            "दिगदी (मोहपूर)",
            "धानोरा (दिगदी)",
            "पाचुंडा",
            "पानोळा",
            "बोरवाडी",
            "मांडवा (माहूर)",
            "मुंगाशी",
            "मेंडकी",
            "रामपूर (मु.) (एन.व्ही.)",
            "वानोळा",
            "वानोळा तांडा",
            "साकूर",
            "अखार्गा",
            "अडळूर",
            "अंबुळगा खु.",
            "अंबुळगा बु.",
            "ईकलारा",
            "ईतग्याळ पी.डी. ",
            "उच्छा बु.",
            "उंद्री पी.डी.",
            "कर्णा",
            "काबनूर",
            "केरुर",
            "कोळगाव",
            "कोळनूर",
            "खाटगाव पी.डी.",
            "गडग्याळवाडी",
            "चांदोळा",
            "जामखेड",
            "जांभळी",
            "तांदळी",
            "धानज",
            "धामणगाव",
            "नांदगाव पी.डी.",
            "बेटमोगरा",
            "बोरगाव",
            "भागनूरवाडी",
            "मकनी",
            "मावळी",
            "मोतर्गा",
            "मंडळपूर",
            "राठोडवाडी",
            "वासूर",
            "साकनूर",
            "सांगवी भादेव",
            "हनगर्गा पी.के.",
            "हिब्बत",
            "होनवडज",
            "ईजळी",
            "चिकाळा",
            "चिकाळा तांडा",
            "माळकौथा",
            "मेंडका",
            "रोही पिंपळगाव",
            "रोही पिंपळगाव तांडा",
            "वर्दाडा",
            "वाडी मुक्ताजी",
            "वाडी मुक्तापूर",
            "हाजापूर",
            "अडगाव",
            "अंबेसांगवी",
            "कांबेगाव",
            "खांबेगाव",
            "चिताळी",
            "जावळा",
            "दापशेड",
            "देऊळगाव",
            "धानोरा (मक्ता)",
            "निळा",
            "पळशी",
            "पांग्री",
            "पिंपळगाव (अयाब)",
            "पिंप्राणवाडी",
            "पोखर भोशी",
            "पोखरी",
            "पोळेवाडी",
            "बेट सांगवी",
            "बेराळी खु.",
            "बोरगाव अकनक",
            "बोरगाव कोल्हा",
            "भेंडेगाव",
            "मडकेवाडी",
            "मासकी",
            "मंगरुळ",
            "सोनमांजरी",
            "हिप्पर्गा (चिताळी)",
            "होत्तळवाडी",
            "अष्टी",
            "उमरी (जा)",
            "उमरी (दर्याबाई)",
            "उंचाडा",
            "कांजरा (खु.)",
            "कांजरा (बु.)",
            "कुसाळवाडी",
            "केदारगुडा",
            "कोप्रा",
            "कोळगाव",
            "कोळी",
            "खार्बी",
            "घोग्री",
            "चोरांबा खु.",
            "चोरांबा बु.",
            "जांभळ सावली",
            "टाकळगाव",
            "डोर्ली",
            "तरोडा",
            "तळंग",
            "ताळेगाव",
            "तोळ्याचीवाडी",
            "निमटोक",
            "नेवरवा़डी",
            "नेवरी",
            "पिंगळी",
            "पिंपळगाव",
            "बोरगाव (हस्तरा)",
            "ब्रम्हवाडी",
            "मार्दगा",
            "मार्लेगाव",
            "येवळी",
            "राजवाडी",
            "वरवट",
            "वळकी बु.",
            "वाई पाना (खु)",
            "वाई पाना (बु)",
            "शिवनी",
            "शिवपूरी",
            "सावरगाव",
            "एकधारी",
            "कार्ला पी.",
            "खैरगाव (जहागिर)",
            "चिंचोर्डी",
            "जिरोना",
            "पार्डी (जाह)",
            "पिंचोडी",
            "महदपूर",
            "मंगरूळ",
            "रामणवाडी",
            "वडगाव (जा)",
            "वडगाव (तांडा)",
            "वासी",
            "शिबादरा (जे)",
            "सावना",
            "अरबूजवाडी",
            "अंतरवेली",
            "आनंदनगर (नविन)",
            "उंडेगाव",
            "काटकरवाडी",
            "कुंडगीरवाडी",
            "कोद्री",
            "खळी",
            "खोकलेवाडी",
            "गौंडगाव",
            "चिलगरवाडी",
            "चिंचटाकळी",
            "डोंगरगाव शेलगाव",
            "डोंगरजावळा",
            "डोंगरपिंपळा",
            "तांदुळवाडी",
            "देवकतवाडी",
            "धारखेड",
            "धारासूर",
            "धेबेवाडी (थाग्याचीवाडी)",
            "नागथाना",
            "पांढरगाव",
            "बडवणी",
            "ब्राम्हणाथवाडी (नविन)",
            "मुळी",
            "मैरळ सावंगी",
            "लिंबेवाडी",
            "लिंबेवाडी तांडा",
            "वागदरा",
            "वाघदरा तांडा (नविन)",
            "शिरसाम शेगाव",
            "सुपा (खालसा)",
            "सुपा (जगीर)",
            "सुपा तांडा (नविन)",
            "असोला",
            "आडगाव खंडागळे",
            "आसेगाव",
            "आंबरवाडी",
            "उम्रड",
            "करंजी",
            "कवडगाव पी आर औंढा",
            "कवडा",
            "कवथा",
            "कावी",
            "कु-हाडी",
            "कोथा",
            "कोरवाडी",
            "कौडगाव पी.जरी",
            "कंथा",
            "गडदगव्हाण",
            "घागरा",
            "चर्थाना",
            "चितनरवाडी",
            "चौधर्णी खु.",
            "जांभुर्ण ",
            "डाभा",
            "ढोपसतवाडी",
            "दहेगाव",
            "दुधगाव",
            "दुधानगाव",
            "देवसढी",
            "धानोरा खु.",
            "धानोरा बु.",
            "नव्हती तांडा",
            "पिर्णपैगाव काजळे",
            "पिंपरी खु.",
            "पिंपळगाव काजळे तांडा",
            "बदनापूर",
            "बेलखेडा",
            "बेलुरा",
            "बोरगळवाडी",
            "ब्रहर्ननगाव",
            "मोळा",
            "मोहाडी",
            "लिंबाळा",
            "वडी",
            "वरुड",
            "वाघी (धानोरा)",
            "वासा",
            "शिवाचीवाडी",
            "साखरतळा",
            "सावरगाव",
            "सावरगाव तांडा",
            "सावंगी भांबळे",
            "सेवालाल नगर",
            "सोन्ना",
            "हनवतखेडा",
            "हंडी",
            "आळंद",
            "आंगलगाव",
            "इरुखा तर्फे पेडगाव",
            "काष्टागाव",
            "किन्होळा",
            "कौडगाव तर्फे सिंगनापूर",
            "गव्हा",
            "जरी",
            "जाडेगाव",
            "जांब",
            "टाकळी बोबडे",
            "ताडलिंबळा",
            "तुळजापूर",
            "थोळा",
            "धरनगाव",
            "धर्मापुरी",
            "धसाडी",
            "नांदखेडा",
            "पन्हेरा",
            "पारळगव्हण",
            "पारवा",
            "पिंगी कोथाळा",
            "पिंपळा",
            "पिंपैगाव तोंग",
            "पेडगाव",
            "पोरजवळा",
            "पंढरी",
            "ब्रम्हापुरी तर्फे पाथरी",
            "ब्रम्हापुरी तर्फे पेडगाव",
            "भरलेले नाही",
            "भोगाव",
            "मांडखळी",
            "मिरखेल",
            "मिर्जापूर",
            "मोहापूरी",
            "लोहगाव",
            "वडगाव तर्फे टाकळी",
            "वरपूड",
            "वाडी दामाई",
            "साडेगाव",
            "साळापुरी",
            "सावंगी खु. ",
            "सिरसी खु.",
            "सिरसी बु.",
            "सुलतानपूर",
            "सोन्ना",
            "हसनापूर",
            "हिंगला",
            "किन्होळा खु.",
            "खेरडा",
            "गोपेगाव",
            "चाटे पिंपळगाव",
            "टाकळगव्हाण",
            "देवनंद्रा",
            "नाथारा",
            "निवळी",
            "पाटोडा गंगा किन्हारा",
            "पाथरगव्हाण खु.",
            "पाथरगव्हाण बु.",
            "पोहे टाकळी",
            "बानेगाव",
            "बाबुळतार",
            "बंदारवाडा",
            "मरदासगाव",
            "मंजारथ",
            "रेनाखळी",
            "रेनापूर",
            "वाघाळा",
            "वाडी",
            "वारखेडा",
            "सरोळा बु.",
            "सारोळा खु.",
            "हदगाव बु.",
            "अंजनवाडी (नविन)",
            "उम्रा",
            "कोळवडी",
            "गिरधारवाडी",
            "गुलखंड",
            "तांबुळगाव",
            "धुप्पा ",
            "पेठ शिवणी",
            "पेंडू खु.",
            "पेंडू बु.",
            "फट्टुनाईक तांडा",
            "फारकंडा",
            "बनवास",
            "मुतखेड",
            "मोजमबाद",
            "मोजमबाद तांडा (नविन)",
            "रामपूर",
            "वाडी (खु.)",
            "वाडी (बु.)",
            "वारखेड",
            "सदलपूर",
            "सरफ्राजपूर",
            "सिपेगाव",
            "सेलू",
            "अळेओन",
            "कलमुळा",
            "कळगाव",
            "कौळगाव",
            "कौळगाववाडी",
            "खरबाडा",
            "गोलेगाव पालम",
            "चांगेफळ",
            "चुडवा",
            "तामकळस",
            "धानोरा काळे",
            "पिंपळ भात्या",
            "पिंप्रन",
            "फुलकळस",
            "बानेगाव",
            "मखनी",
            "महागाव",
            "माहेर",
            "मुंबर",
            "वजूर",
            "वडगाव तर्फे नवकी",
            "सिरकळस",
            "सुरवडी",
            "हटकरवाडी",
            "अटोला",
            "इटळी",
            "उक्कलगाव",
            "केकर जावजा",
            "कोथाळा",
            "कोल्हा",
            "खरबा",
            "तहर",
            "ताड बोरगाव",
            "देऊळगाव आवचर",
            "नागर जावळा",
            "नारलद",
            "पार्डी (पी.टाकळी)",
            "पाळोदी",
            "बोंडरवाडी",
            "मानवत रोड",
            "रत्नापूर",
            "राजूरा",
            "वजुर खु.",
            "वजुर बु.",
            "शेवडी जहागीर",
            "सोमथाना",
            "अरसद",
            "अहेर बोरगाव",
            "कुप्टा",
            "कुंडी",
            "खवने पिंपरी",
            "खेर्डा दुधाना किनारा",
            "गव्हा",
            "गुगली धामणगाव",
            "गुळखंड",
            "गोहेगाव",
            "चिखलतनाना बु.",
            "चिखलथाना खु.",
            "जावळा जिवजी",
            "तळतुंबा",
            "तांदुळवाडी",
            "तिडी पिंपळगाव",
            "देऊळगाव गाट",
            "नागथाना",
            "ब्राम्हणगाव पारगने परतूर",
            "भंगापूर",
            "मोरेगाव",
            "म्हाळसापूर",
            "रावळगाव",
            "लाडनंद्रा",
            "साळेगाव",
            "सावंगी पीसी",
            "सिमनगाव",
            "सोनवटी",
            "हत्ता",
            "हिस्सी",
            "कोथाळा",
            "गोलेगाव",
            "डिघोळ इस्लामपूर",
            "थडीउक्कडगाव",
            "थडी पिंपळगाव",
            "दुधगाव",
            "धामोनी",
            "नारवाडी",
            "बोंडरगाव",
            "लसिना",
            "वनीसंगम",
            "वाघलगाव (ल)",
            "वाडी पिंपळगाव",
            "विटा खु. ",
            "अकोला",
            "उमरई",
            "कुंबेफळ",
            "कोडरी",
            "कोळकंडी",
            "घाटनांदूर",
            "चिचखंडी",
            "ताडोळा",
            "दिघोळ आंबा",
            "देवळा",
            "दैथाना रडी",
            "धानोरा बु.",
            "नांदगाव",
            "नंदादी",
            "पाटोडा",
            "माकेगाव",
            "मामदापूर (परळी)",
            "मामदापूर (पाटोडा)",
            "माळेवाडी",
            "मुदेगाव",
            "येळदा",
            "रक्षावाडी",
            "रडी",
            "राडि तांडा (न.व.)",
            "लोखंडी सावरगाव",
            "वरपगाव",
            "वाघाळा (रडी)",
            "शिरपत्रईवाडी",
            "साटेफळ",
            "सायगाव",
            "साँगओन",
            "सुगाव",
            "सेलू अंबा",
            "सोमनाथ बोरगा",
            "हिवरा खु.",
            "अष्टा",
            "अंधळेवाडी",
            "कडा",
            "कनाडी बु.",
            "करंजी",
            "क-हेवडगाव",
            "क-हेवाडी",
            "कारखेड बु.",
            "केरुळ",
            "खळतवाडी",
            "खानापूर",
            "गंगेवाडी",
            "गंधानवाडी",
            "चिखली",
            "चिंचपूर",
            "जळगाव",
            "टाकळसिंग",
            "देवीगव्हाण",
            "देसूर",
            "धिरडी",
            "निमगाव चौभा",
            "पांगूळगव्हाण",
            "पांढरी",
            "पिंपरखेड",
            "पिंपरी घुम्री",
            "पिंपळसुत्ती",
            "बीड-सांगवी",
            "बेळगाव",
            "भातोडी",
            "भाळोनी",
            "भोजेवाडी",
            "मतकुळी",
            "मतवळी",
            "मांडवा",
            "मुरशादपूर",
            "मोरेवाडी",
            "मंगरुळ",
            "रीक्त",
            "रुटी",
            "वतनवाडी",
            "वनवेवाडी",
            "वाकी",
            "शेरी खु.",
            "शेरी बु.",
            "शेलारवाडी (नविन)",
            "सांगवी अष्टी",
            "सांगवी पाटन",
            "सुरुडी",
            "सुलेमान देवळा",
            "सोळेवाडी",
            "हजीपूर",
            "हनुमंतगाव",
            "हिवरा",
            "हिंगनी",
            "अरनगाव",
            "अंधळे-वाडी",
            "एकुरका",
            "कापरेवाडी",
            "काळेगाव घाट",
            "कासारी",
            "कांडी बदन",
            "कांडी माळी",
            "केळगाव",
            "कोरडेवाडी",
            "गोतेगाव",
            "जोळा",
            "डेपेवडगाव",
            "तांबवा",
            "देवगाव",
            "धरमाळा",
            "पळसखेडा",
            "पिसेगाव",
            "पिंपळगव्हाण",
            "बेळगाव",
            "बोबडेवाडी",
            "बोरीसावरगाव",
            "भोपाळा",
            "मस्साजोग",
            "माळेगाव",
            "मुळेगाव",
            "मोटेगाव",
            "लाडेगाव",
            "लाडेवडगाव",
            "वरपगाव",
            "विडा",
            "शिंधी",
            "शेलगाव गंजी",
            "सरुळ",
            "सर्नी (सांगवी)",
            "ससुरा",
            "साबळा",
            "साळेगाव",
            "सांगवी (एस)",
            "सुकळी",
            "सोनेसांगवी",
            "हनुमंत पिंपरी",
            "होळ",
            "अडपिंपरी",
            "अमला",
            "अर्धपिंपरी",
            "अर्धमासळा",
            "अंतरवळी बु.",
            "आगर नांदुर",
            "उमापूर",
            "औरंगपूर जवळका",
            "काजळ्याचीवाडी",
            "काथोडा",
            "काथोडा तांडा (नविन)",
            "कांबी माजरा",
            "कोलतेवाडी",
            "खामगाव",
            "खेर्डा बु.",
            "खेर्डावाडी",
            "गायकवाड जळगाव",
            "गुळज",
            "गोळेगाव",
            "गोळेगाव तांडा (नविन)",
            "चाकळंबा",
            "जळगाव (माजरा)",
            "जातेगाव",
            "ठाकर अडगाव",
            "ढुमेगाव",
            "देवकी",
            "धानोरा",
            "नागझरी",
            "नांदपूर",
            "नांदलगाव",
            "निपानि जवळका",
            "पडूळ्याचीवाडी",
            "पांढरवाडी",
            "पंचाळेश्वर",
            "पंढरी",
            "बागपिंपळगाव",
            "बेलगुडवाडी",
            "भेंड खु.",
            "भेंड टाकळी",
            "भेंड बु.",
            "मनुबाई जावळा",
            "मन्यारवाडी",
            "महार टाकळी",
            "मालेगाव खु.",
            "मालेगाव बु.",
            "मालेगाव माजरा",
            "मिरगाव",
            "राक्षसभवन",
            "रामेश्वर",
            "रांजनी",
            "रीक्त",
            "रुई",
            "लोनाळा",
            "लोनाळा तांडा (नविन)",
            "वहेगाव अमला",
            "शिंदेवाडी",
            "सिरसदेवी",
            "सिंदफनाचिंचोली",
            "सुजानपूर",
            "सुरळेगाव",
            "सुलतानपूर",
            "अस्वला",
            "अंजनडोह",
            "आवरगाव",
            "आसरडोह",
            "उमरेवाडी",
            "कान्नापूर",
            "कुंडी",
            "कोळपिंपरी",
            "तांदळवाडी",
            "पांगरी",
            "मुंगी",
            "मोरफळी",
            "रीक्त",
            "रुई धारुर",
            "सुकळी",
            "हसनाबाद",
            "कवळ्याचीवाडी",
            "कारेवाडी",
            "गारडेवाडी",
            "गोपाळपूर",
            "ताडोळी",
            "नागपिंपरी",
            "परचुंडी",
            "बोधेगाव",
            "भिलेगाव",
            "मरळवाडी",
            "मलकापूर (नविन)",
            "मळनाथपूर",
            "माळहिवरा",
            "मिरवत",
            "मैंदवाडी",
            "मोहा",
            "मंडेखेल",
            "रीक्त",
            "लेंडवाडी",
            "वडखेड",
            "वाघाळा",
            "सरफराजपूर",
            "सेलू",
            "सेलू-परळी",
            "सोनहिवरा",
            "अमळनेर",
            "आंबेवाडी",
            "कारेगाव",
            "कोतन",
            "गांधनवाडी",
            "घोलेवाडी (नविन)",
            "चांदेरवाडी",
            "चिंचोली",
            "जन्याचीवाडी (नविन)",
            "जाधववाडी (नविन)",
            "जिरेवाडी",
            "डागाचीवाडी",
            "डोंगरकिन्ही",
            "ढोपरवाडी (नविन)",
            "तुपेवाडी",
            "दौलतवाडी",
            "नळवंडी",
            "नागेशवाडी",
            "निवडूंगा",
            "पवारवाडी (सरदवाडी)",
            "पांढरवाडी",
            "पिंपळवंडी",
            "भाटेवाडी",
            "मळेकरवाडी",
            "मांडवेवाडी",
            "मिसाळवाडी",
            "राऊतवाडी (नविन)",
            "रीक्त",
            "साबळेवाडी",
            "अडगाव",
            "अहेर धानोरा",
            "अहेर निमगाव",
            "अहेर वडगाव",
            "ईत",
            "ईमामपूर",
            "औरंगपूर",
            "कुकडगाव",
            "कुटेवाडी (नविन)",
            "कुर्ला",
            "कोल्हारवाडी",
            "खुंड्रास",
            "गुंजाळा",
            "गुंढा",
            "गुंढेवाडी",
            "गंगानाथवाडी",
            "जरूड",
            "जुज गव्हाण",
            "नळवंडी",
            "नाथापूर",
            "पाली",
            "पिंपळगाव (माजरा)",
            "बहदरपूर",
            "बाभळ खुंटा",
            "बाभळवाडी",
            "बेलापूरी",
            "ब्रम्हागाव",
            "भवनवाडी",
            "भांदरवाडी",
            "भैरवाडी",
            "मानझरी (हवेली)",
            "मेंगडेवाडी",
            "म्हळसापूर",
            "म्हाळस जावळा",
            "राक्षस भुवन",
            "राजकपूर",
            "रामगाव",
            "रांजेगाव",
            "रीक्त",
            "लोणी (शहाजानपूर)",
            "लोळदगाव",
            "वडगाव गुंढा",
            "वरवती",
            "वांगी",
            "शहाजानपूर (लोणी)",
            "शिडोदे",
            "शिवनी",
            "सक्षळ पिंपरी",
            "सामनापूर",
            "ईरला माजरा",
            "एकदरा",
            "किट्टी आडगाव",
            "केसापूरी",
            "खेर्डा खु.",
            "गोविंदवाडी",
            "जदीद जवळा",
            "डावरगाव खु.",
            "डेपेगाव",
            "तालखेड",
            "तेलगाव खु.",
            "दुब्बा माजरा",
            "धनगरवाडी (पायटलवाडी)",
            "फुल पिंपळगाव",
            "फुले पिंपळगाव",
            "भाटवडगाव",
            "मनुरवाडी",
            "मंगरुळ",
            "मंजरथ",
            "रामपिंपळगाव",
            "रीक्त",
            "शहापूर माजरा",
            "सादोळा",
            "हरकी निमगाव",
            "उपळी",
            "कान्होबाचीवाडी",
            "कुप्पा",
            "खडकी",
            "चिंचाळा",
            "डावरगाव बु.",
            "तिगाव",
            "दुकडेगाव",
            "देवळा बु.",
            "पुसरा",
            "बावी",
            "मोरवड",
            "रीक्त",
            "रुई पिंपळा",
            "सोन्नाखोटा",
            "ऊखळवाडी",
            "कलमेश्वर धानोरा",
            "खरमातवाडी  (नविन)",
            "खालापूरी",
            "खोकेमोहा",
            "खोप्ती",
            "खंबा",
            "गोमळवाडा",
            "घुगेवाडी",
            "जतनांदुर",
            "जेधेवाडी",
            "पांग्री",
            "पिंपळनेर",
            "पौंडूळ",
            "बरगजवाडी",
            "भाडकेल",
            "मदमापूरी",
            "मळकाचीवाडी",
            "मोरजाळवाडी",
            "रीक्त",
            "लिंबा",
            "वडाळी",
            "सावसवाडी",
            "हिवरसिंगा",
            "अकोली",
            "अटाळी",
            "अवर",
            "उम्रा अटाळी",
            "कवडगाव",
            "कासारखेड",
            "किन्ही महादेव",
            "कुंभेफळ",
            "कोळोरी",
            "खेरडी",
            "चिंचखेड बंड",
            "जळका तेळी",
            "टाकळी",
            "धोरपगाव",
            "नागापूर",
            "निलेगाव",
            "पाटोंडा",
            "पिंपरी गवळी",
            "पिंपरी मोहदर",
            "पिंपळगाव राजा",
            "पिंपळचोच",
            "पिंप्राळा",
            "पेडका",
            "बेळखेड (नविन)",
            "भालेगाव",
            "भेंडी",
            "रामनगर",
            "वडजी",
            "विहीगाव",
            "हिंगणा कारेगाव",
            "अमोना",
            "अंचरवाडी",
            "अंत्री कोली",
            "इसरुळ",
            "कोनड",
            "देऊलगाव धनगर",
            "बोराला",
            "भोकर",
            "भोर्सा",
            "माळगनी",
            "माळगी",
            "माळशेंबा",
            "मिसाळवाडी",
            "मंगरुळ प्र. खेरडा",
            "येवता",
            "रामनगर",
            "वसंतनगर",
            "वाघापूर",
            "शेलगाव अटोल",
            "साकेगाव",
            "सावरगाव दुकरे",
            "अकोला खु.",
            "अडोळ खु.",
            "अडोळ बु.",
            "असलगाव",
            "इलोरा",
            "इस्लामपूर",
            "उमापूर",
            "कुरनगड खु.",
            "कुरनगड बु.",
            "खांडवी",
            "खेर्डा खु.",
            "खेल पारसकर (जामोड)",
            "खेळशिवापूर (जामोड)",
            "गाडागाव खु.",
            "गाडेगाव बु.",
            "गोळेगाव खु.",
            "गोळेगाव बु.",
            "गौळखेड",
            "चावरा",
            "झडेगाव",
            "टाकळी खाटी",
            "टाकळी खासा",
            "टाकळी परासकर",
            "तरोडा बु.",
            "तिवडी अजमपूर",
            "ददुलगाव",
            "दौतपूर",
            "निमकराड",
            "निंभोरा खु.",
            "निंभोरा  बु.",
            "परशारामपूर",
            "पळशी घाट",
            "पळशी वैद्य",
            "पळसखेड",
            "बेंडवड खु.",
            "बेंडवड बु.",
            "बोराळा खु.",
            "महूळी",
            "मादाखेड खु.",
            "मानेगाव",
            "मांडवा",
            "राजूरा खु.",
            "राजूरा बु.",
            "रीक्त",
            "वडशिंगी",
            "सुनगाव",
            "हशमपूर",
            "हिंगना प्र.बलापूर",
            "चिंचखेड",
            "दोद्रा",
            "पिंपरी अंधाळे",
            "बैगाव प्र.खेरडा",
            "मंडपगाव",
            "सुलतानपूर",
            "अहमदपूर",
            "आलमपूर",
            "इसरखेडा",
            "औढा खु.",
            "औरंगपूर",
            "कोकळवाडी",
            "कोदरखेड",
            "खडतगाव",
            "खारखुंडी",
            "खुडावंतपूर",
            "खुमगाव",
            "खेडगाव",
            "खेर्डा",
            "खैरा",
            "खंडाळा",
            "गोंधनखेड",
            "घोरधाडी",
            "चांदूर बिसवा",
            "चिंचखेड प्र.मलकापूर",
            "जावळा बजार",
            "जीगाव",
            "टाकरखेड",
            "टाकळी (वापळ)",
            "टाका",
            "दहिवडी",
            "दिघी",
            "धाडी",
            "धानोरा बु.",
            "नारखेड",
            "नांदुरा खु",
            "निमगाव",
            "पळसोडा",
            "पातोंडा",
            "पिंपरी आढाव",
            "पिंपरीकोळी",
            "पिंपळखुटा धांडे",
            "फुळी",
            "बरफगाव",
            "बेलद प३. जळगाव",
            "बेलुरा",
            "भुशिंग",
            "मातोडा",
            "मामुळवाडी",
            "मालेगाव प्र.पि.राजा",
            "मुरंबा",
            "मेंधळी",
            "मोमिनाबाद",
            "येरली",
            "रसुलपूर प्र.राजा",
            "रीक्त",
            "लोनवाडी प्र. नांदुरा",
            "वडनेर",
            "वळती बु.",
            "वाडली",
            "वाडी प्र.वडनेर",
            "वासाडी बु.",
            "शेलगाव मुकुंद",
            "शेंबा बु.",
            "सानपुडी",
            "सावरगाव चाहू",
            "सावरगाव नेहू",
            "सांगवा",
            "हिंगणा गाव्हद",
            "अवलखेड",
            "अंभोडा",
            "इस्माईलपूर",
            "कोळवड",
            "चिखला",
            "झरी",
            "तांदुळवाडी",
            "दिपूर",
            "दुधा",
            "नांद्राकोळी",
            "भाडोळा",
            "सागवन",
            "हतेदी खु.",
            "हतेदी बु.",
            "उमळी",
            "कम्रदीपूर",
            "कुंद बु.",
            "कोरवड",
            "खडकी",
            "खापरखेड",
            "खामखेड प्र. मलकापूर",
            "खोकोडी",
            "गहूळखेड",
            "गाडेगाव",
            "गोरद",
            "घिरणी",
            "घोदी",
            "चिंचोळ",
            "जांभुळढाबा",
            "तांदुळवाडी प्र.मलकापूर",
            "तिग-हा प्र.मलकापूर",
            "तेलखेड",
            "दताळा",
            "दसरखेड",
            "दुढळगाव खु.",
            "दुढळगाव बु.",
            "देवढाबा",
            "नारवेळ",
            "निंबारी",
            "पिंपळखुंटा (महादेव)",
            "बळद प्र.मलकापूर",
            "भादगनी",
            "भाळेगाव",
            "भांगुरा",
            "मकनेर",
            "मलकापूर (रुरल)",
            "रनगाव",
            "रनथाम",
            "रास्तापूर",
            "रीक्त",
            "लहे खु.",
            "लोनवाडी प्र.मलकापूर",
            "वरखेड",
            "वाकोडी",
            "वाघोला",
            "विवारा",
            "शिवनी",
            "सिरढोन",
            "हरसोडा",
            "हिंगना काझी",
            "हिंगना नागापूर",
            "उद्धव (नविन)",
            "गाजरखेड",
            "घाटबोरी",
            "चिंचाळा",
            "जानूना",
            "जांभरुन",
            "टेंभूरखेड",
            "देगाव",
            "द्रुगबोरी",
            "पाथर्डी",
            "बोथा",
            "भोसा",
            "वरुड",
            "वागदेव",
            "शिवाजी नगर",
            "शेलगाव काकडे",
            "हिवरा बु.",
            "अजादरद",
            "अंत्री",
            "कबरखेड",
            "काळेगाव प्र.रोहीनीखेड",
            "चवर्दा",
            "ढोनखेड",
            "तपोवन",
            "थाड",
            "दुधामाळ",
            "पिंपरी गवळी",
            "पुन्हई",
            "पोखरी",
            "पोफळी",
            "फरदापूर",
            "मोयखेड",
            "रोहीनीखेड",
            "वडगाव प्र.रोहीनीखेड",
            "वाडी",
            "सरोळा पीर",
            "सारोळा (मारोती)",
            "संगळद प्र.राजूर",
            "हनवतखेड",
            "कुंडळस",
            "कौलखेड",
            "गोत्रा",
            "जांबूळ",
            "टिटवी",
            "देऊळगाव वैसा",
            "नांद्रा",
            "पार्था",
            "पांगरडोळा",
            "पिंपळनेर",
            "भिवापूर",
            "रैगाव",
            "वढाव",
            "शिवनी जत",
            "सोनुना",
            "अडसूळ",
            "अलासना",
            "उन्हाळखेड",
            "एकफळ",
            "कनारखेड",
            "कळवड",
            "काथोरा",
            "काळखेड",
            "कुरखेड",
            "खातखेड",
            "खेर्डा",
            "गऊळखेड",
            "गव्हाण",
            "गैगाव खु.",
            "गैगाव बु.",
            "गोलेगाव खु.",
            "गोलेगाव बु.",
            "चिंचखेड",
            "चिंचोली करफर्मा",
            "जळंब",
            "जानोरी",
            "जावळा पळसखेड",
            "जावळा बु.",
            "झाडेगाव",
            "टाकळी धराव",
            "टाकळी नागझरी",
            "टाकळी हत",
            "डोंगरखेड",
            "तिव्हन खु.",
            "तिव्हन बु.",
            "दोलरखोड",
            "पडसूल",
            "पहूरजिरा",
            "पाळोदी",
            "ब्राम्हणवाडा",
            "भसतन",
            "भोंगगाव",
            "भोंडगाव",
            "मचिंद्रखेड",
            "महागाव",
            "माजलपूर",
            "मानसगाव",
            "मानेगाव",
            "येऊलखेड",
            "रीक्त",
            "लासुरा बु.",
            "लोनटेक",
            "वरखेड बु.",
            "वरध",
            "वरुड",
            "शेगाव (आर)",
            "सागोडा",
            "सावर्णा",
            "सांगवा",
            "हिंगना वैजानाथ",
            "उम्रड",
            "खामगाव",
            "गुंजी",
            "जयपूर",
            "जांभोरा",
            "ताडशिवनी",
            "तांदुळवाडी",
            "पांगरखेड",
            "पांगरी उगळे",
            "बोरखेडी जलाल",
            "महारखेड",
            "राहेरी बु.",
            "वरुडी",
            "वाघोरा",
            "शिवनी टाका",
            "शेवगा जहागीर",
            "सारखेड",
            "सावरगाव माळ",
            "सोखेड तेजन",
            "हानवटखेड",
            "अटकळ",
            "अवर",
            "अस्वंद",
            "अंबाबरवा",
            "इतखेड",
            "उकडगाव",
            "उकळी बु.",
            "कळमखेड",
            "कवथाळ",
            "काकोडा",
            "काटेड",
            "किली पिंपळडोळ",
            "कुंढेगाव",
            "कुंबरखेड",
            "कोद्री",
            "कोलाड",
            "खिरोदा",
            "चांगेफळ खु.",
            "चिंचखेड",
            "चुनखेडी",
            "चोंडी",
            "जस्तगाव",
            "टाकळी पंचगव्हाण",
            "टाकळेश्वर",
            "तामगाव",
            "तुंकी खु.",
            "दुर्गादित्य",
            "देऊळगाव",
            "निरोड",
            "निवाना",
            "नेकनापूर",
            "पळशी झासी",
            "पळसोडा",
            "पातुर्डा खु.",
            "पिंपरी अडगाव",
            "पेसोडा",
            "बनोडा एकलारा",
            "बावनबीर",
            "बोडखा",
            "भिलखेड",
            "भोन",
            "मनार्डी",
            "रीक्त",
            "रींगणवाडी",
            "रूधाना",
            "रोहिन खिंडकी",
            "लडनापूर",
            "वडगाव प्र.अडगाव",
            "वनखेड",
            "वाकना",
            "वारवत खंडेराव",
            "वारवत बकळ",
            "सालवन",
            "सावळी",
            "सोनाळा",
            "हिंगणा कवथळ",
            "अंजरगाव",
            "काथोडा",
            "कु-हा डू.",
            "केळझरा",
            "कोपरा",
            "कोळवन",
            "चिखली",
            "देउरवाडी (आर.एच.व्ही)",
            "पांढुर्णा",
            "पिंपळनेर",
            "भंडारी ज.",
            "भंसारा",
            "मंगरुळ",
            "येरमाळ",
            "सुकळी",
            "अमनपूर",
            "अमळा",
            "आमदरी",
            "आंबवन",
            "कारोडी",
            "काळेश्वर",
            "कैलास नगर",
            "चिल्ली",
            "चिंचोली संगम",
            "चुरमुरा",
            "डिंदाळा",
            "दाहगाव",
            "नागेशवाडी",
            "बित्तरगाव",
            "बोथा",
            "मरळेगाव",
            "मरसूळ",
            "रंगोली",
            "लिमगव्हाण",
            "वरुड बिबी",
            "श्री दत्तनगर",
            "सुकळी (जहागीर)",
            "एकलसपूर",
            "औरंगपूर",
            "कळंब",
            "कसमपूर",
            "कामथवाडा",
            "खुटाळा",
            "गनमगाव",
            "गंगादेवी",
            "गंधा",
            "घोटी",
            "चापर्डा",
            "जोंधळनी",
            "डोंगरखर्डा",
            "दत्तपूर",
            "दौलतपूर",
            "निमगव्हाण",
            "निळज",
            "पिंपळशेंडा",
            "पोटगव्हाण",
            "बेलोना",
            "मनकापूर",
            "मलकापूर",
            "मवळनी",
            "महितापूर",
            "मुबारकपूर",
            "रीक्त",
            "वांदळी",
            "शिंगापूर",
            "साटेफळ",
            "सोनखस",
            "सोनेगाव",
            "हिरापूर",
            "जिरा",
            "जोगिनकावडा",
            "टाटापूर",
            "दाभा",
            "पाथरी",
            "पिंपरीरोड",
            "मिरा",
            "रूंझा",
            "वथोडा",
            "शामपूर",
            "अमडी",
            "अंजी (एन)",
            "किन्ही",
            "कु-हड",
            "कुंभारी",
            "कोपरी",
            "जुनोनी",
            "तिवसाळा",
            "दहेगाव",
            "निमबरडा",
            "पांगडी",
            "पांडुर्णा खु.",
            "पांडुर्णा बु.",
            "पिंपरी",
            "मनोळी",
            "सासनी",
            "अडकोळी",
            "अर्धवन",
            "कमलपूर",
            "खापरी",
            "गणेशपूर खु.",
            "गोविंदपूर",
            "पवनार",
            "पारडी",
            "पांढरकवडा",
            "पिंपरड",
            "बैलमपूर",
            "भेंडळा",
            "मर्की खु.",
            "मर्की बु.",
            "मांगळी",
            "राजूर",
            "सावळी",
            "हिरापूर",
            "अंतरगाव",
            "उमरी (एल)",
            "कु-हाड बु.",
            "खेड",
            "जावळा",
            "टाकळी  बु.",
            "डोल्हारी",
            "तारोडा",
            "देऊळगाव",
            "दोब",
            "पळशी",
            "ब्रम्हनाथ",
            "रामगाव",
            "लख्खींद",
            "सिंधी",
            "हरु",
            "अष्टा",
            "काळगाव",
            "कंदाळी",
            "देहनी",
            "दोल्हारी",
            "दोळंबवाडी",
            "दोळंबा",
            "पिंपरी",
            "महागाव",
            "वडगाव",
            "वरंदळी",
            "वसंतनगर",
            "विठाळा",
            "अजनी",
            "अडगाव",
            "उमरथा",
            "कन्हेरगाव",
            "कारखेडा",
            "खरडगाव",
            "खळाना",
            "खानापूर",
            "खोलापूरी",
            "घारेफूल",
            "चिकनी",
            "टेंभी",
            "डोनद",
            "डोमगा",
            "पर्जना",
            "पंढरी",
            "ब्राम्हणवाडा (पी)",
            "भाळकी",
            "महाजनपूर",
            "रामगाव",
            "व्यहळी",
            "शिरजगाव",
            "साटेफळ",
            "सावरगाव",
            "असोली",
            "कोंदई",
            "खडकदरी",
            "खारशी",
            "दहीवड बु.",
            "धरनवाडी",
            "पाळू",
            "पिंपरवाडी",
            "पोखरी",
            "बालवाडी (नविन)",
            "बिबी",
            "येहळा",
            "लोंदरी",
            "वरवत",
            "वेणी खु.",
            "हर्शी",
            "अष्टारामपूर",
            "इसापूर",
            "कोटंबा",
            "कोंधा",
            "नागरगाव",
            "नैगाव",
            "मदानी",
            "मनकापूर",
            "मरळपूर",
            "मुस्ताबाद",
            "रेनकापूर",
            "वरुड",
            "वातखेड खु.",
            "सौजना",
            "कवथा जहागीर",
            "काळी (टेंभी)",
            "कासरबेहेळ",
            "टेंभी",
            "थर खु.",
            "थर बु.",
            "दगड थार",
            "धारकान्हा",
            "पिपंळगांव (ई)",
            "भांब",
            "वरोडी",
            "वाडवळ",
            "सेवनगर",
            "खडकी",
            "खानदनी",
            "खाप्री",
            "खेकडवाई",
            "गोडबुरंदा",
            "घोगुलदरा",
            "घोडदरा",
            "चिंचोनी बोतोनी",
            "धानपूर",
            "मेंढनी",
            "शिवनळा",
            "सराटी",
            "हातवंजारी",
            "कामथवाडा",
            "कारेगाव",
            "किन्ही",
            "कोळंबी",
            "खैरगाव",
            "गोधनी",
            "घाटना",
            "घोडखिंडी",
            "चापडोह",
            "चौधरा",
            "जांब",
            "झुली",
            "डोर्ली",
            "धानोरा",
            "धामनी",
            "निळोना",
            "पांगरी",
            "पांढरी",
            "पिंपरी (बुटी)",
            "पौनमरी",
            "बारबडा",
            "बोथगव्हाण",
            "बोरगाव",
            "माडकोना",
            "मुरझडी",
            "मुरझाडी (चिंच)",
            "मुरझाडी (लाल)",
            "मोहा",
            "मंजार्डा",
            "येवती",
            "रातचंदाना",
            "राहूलघारी",
            "रोहाना",
            "लोणी",
            "वरझाडी",
            "वारूड",
            "सावरगड",
            "हतगाव",
            "इब्राहीमपूर",
            "कळमनेर",
            "कोळवन",
            "खेमकुंड",
            "गोपाळनगर",
            "चिकना",
            "चोंढी",
            "जळका",
            "दापोरी",
            "पिंपरी दुर्ग",
            "पिंपळखुटी",
            "बोरखडी",
            "भांब",
            "भिमसेनपूर",
            "मळकी",
            "मांडवा",
            "मुधापूर",
            "रत्नापूर",
            "रामतिर्थ",
            "राळेगाव",
            "रावेरी",
            "लखापूर",
            "वडजाई",
            "वळधूर",
            "व-हा",
            "वार्णा",
            "श्रीरामपूर",
            "सादंगी (पेरका)",
            "हिवरा",
            "हिवरी",
            "उकनी",
            "कायर",
            "कुंभारी",
            "खुंदरा",
            "चिंचोली",
            "चेंडकापूर",
            "जुनाडा",
            "नवरगाव",
            "नायगाव बु.",
            "निळजाई",
            "पिंपळगाव",
            "पिंप्री",
            "पुरड",
            "पंचधर",
            "बाबापूर",
            "बेलोरा",
            "बोरगाव (मे)",
            "महंकाळपूर",
            "वडजापूर",
            "अंधोरी",
            "उजना",
            "उमरगा येल्लादेवी",
            "कुमठा (बु)",
            "कौडगाव",
            "खराबवाडी",
            "खांडळी",
            "गाडेवाडी",
            "गुडळेवाडी",
            "गंगाहिप्पर्गा",
            "चिळखा",
            "चोपळी",
            "टाकळगाव (सेंकुड)",
            "ढालेगाव",
            "ढासवाडी",
            "नागझरी",
            "पार",
            "बाबळदरा",
            "बोडखा",
            "मकनी",
            "मोरेवाडी",
            "मोळवन",
            "येरतर",
            "रळगा",
            "रुई",
            "वडारवाडी",
            "वाईगाव",
            "वंजारवाडी",
            "शेंद्री",
            "सय्यदपूर (खु.)",
            "सांगवी (सुनेगाव)",
            "सुनेगाव (शेंद्री)",
            "सुमठाणा",
            "अर्सनाळ",
            "कारखेळी",
            "कुमठा खु.",
            "कुमडळ",
            "कुंभाल (नेर)",
            "कौळखेड",
            "गुर्धाळ (उदगीर)",
            "चांडेगाव",
            "चिघळी",
            "जनापूर",
            "तडळापूर",
            "तिवतग्यळ",
            "तोंडचीर",
            "तोंडर",
            "दावनगाव",
            "देवर्जन",
            "धोतरवाडी",
            "बनशेळकी",
            "मडाळपूर",
            "मलकापूर",
            "मोर्तळवाडी",
            "रुद्रवाडी (नविन)",
            "लिंबगाव",
            "लोणी",
            "लोहारा",
            "वाघदरी",
            "वंजारवाडी (नविन)",
            "शिरोळ",
            "शेकापूर",
            "शेलहळ",
            "हल्ली",
            "हैबतपूर",
            "अपचुंडा",
            "अशिव",
            "कवठा लातूर",
            "किनीथोट",
            "किल्लारी",
            "कुमठा",
            "गुबळ",
            "गोटेवाडी",
            "चळबुर्गा",
            "चिंचोली जो",
            "चिंचोली सोन",
            "जवळगा पी डी",
            "जावळी",
            "जैनगर",
            "तळनी लामन तांडा",
            "तुंगी खु.",
            "तुंगी बु.",
            "तोंडोळी",
            "दापेगाव",
            "देवंगा",
            "नागरसोगा",
            "नांदुर्गा",
            "फत्तेपूर",
            "बोरफळ",
            "भांगेवाडी",
            "माळकोंडजी",
            "माळुब्रा",
            "मासळगा खु.",
            "मुगळेवाडी",
            "मंजरुळ",
            "येळवत",
            "येळी",
            "राजेवाडी",
            "लमजना",
            "लोडगा",
            "वाघोली",
            "वानवाडा",
            "सार्नी",
            "सिरसळ",
            "सिंदळवाडी",
            "सिंदळा लोहारा",
            "संक्रळ",
            "हरेगाव",
            "हसळगन",
            "होळी",
            "अलगरवाडी",
            "अष्टा",
            "अंजनसोडा बु.",
            "अंबुळगा",
            "कळकोटी",
            "घारोळा",
            "घार्नी",
            "टाकळगाव (शेळगाव)",
            "तिर्थवाडी",
            "देवंग्रा",
            "देवंग्रावाडी",
            "नागदरवाडी",
            "नाळेगाव",
            "बोथी",
            "बोरगाव बु.",
            "ब्रहमवाडी",
            "भाकरवाडी",
            "माशनेरवाडी",
            "मोहदळ",
            "मंडुर्की",
            "लिंबळवाडी",
            "वडवळ नागनाथ",
            "शिरनळ",
            "सुगाव",
            "हनमंतवाडी",
            "कारंजी",
            "कुंकी",
            "केकट सिंडगी",
            "कोनाळी डोंगर",
            "कोळनूर",
            "जागळपूर बु.",
            "जिर्गा",
            "डोमगाव",
            "ढोरसांगवी",
            "धामनगाव",
            "येळदरा",
            "लाळी बु.",
            "विरळ",
            "सोनवाळा",
            "हावरगा",
            "कमरोद्दीनपूर",
            "कमालवाडी",
            "कोनळी (एन)",
            "गुर्धळ (हेर)",
            "डोंगरेवाडी",
            "ढानेगाव",
            "तळेगाव (भोगेश्वर)",
            "दावन हिप्पर्गा",
            "देवणी बु.",
            "नागरळ",
            "नेकनळ",
            "विलेगाव",
            "संगम",
            "हेळंब",
            "हंचनळ",
            "अंबुळगा बु.",
            "अंबुळगा मेन",
            "आनंदवाडी",
            "आनंदवाडी अंबुळगा बु.",
            "आनंदवाडी (गौर)",
            "आनंदवाडी (जे)",
            "आंबेवाडी अंबुळगा बु.",
            "आंबेवाडी मासळगा",
            "उमरगा (हडगा)",
            "केदारपूर",
            "गौर",
            "चिंचोली (पान)",
            "चिंचोली (भा)",
            "जेवरी",
            "जैनूर",
            "जोतवाडी",
            "झारी",
            "ताडमुंगळी",
            "तांबळवाडी",
            "तांबळा",
            "तुपडी",
            "दगडवाडी",
            "दाडगी",
            "धानोरा",
            "नाडीवाडी",
            "नितूर",
            "पिंपळवाडी (जे)",
            "पीरवाडी",
            "बरमाचीवाडी",
            "बामनी",
            "भुटमुगळी",
            "मासळगा",
            "येळनुर",
            "येळमवाडी",
            "लिंबाळा",
            "शिरुर",
            "शिवनी (को)",
            "शेळगी",
            "सोनेसांगवी",
            "हडगा",
            "हनमंतवाडी (मुगाव)",
            "हळगरा",
            "अंदळगाव",
            "ईत्ती",
            "ईंदरठाणा",
            "कुंभारी",
            "खानापूर",
            "दिघोळ देशपांडे",
            "दिघोळ देशमुख",
            "नागापूर",
            "पळसी",
            "पोहरेगाव",
            "पोहरेगाव तांडा (नविन)",
            "भोकरंबा",
            "मोरवड",
            "लखमपूर",
            "वांगदरी",
            "शेरा",
            "सांगवी",
            "सिंधगाव",
            "सेवालालनगर",
            "अखरवई",
            "कातपूर",
            "कारकाटा",
            "काव्हा",
            "खाडगाव",
            "खांडापूर",
            "खोपेगाव",
            "खंडाळा",
            "गंगापूर",
            "चांडेश्वर",
            "जवळा बु.",
            "जेवळी",
            "ताडकी",
            "दगडवाडी",
            "नागझरी",
            "पाखरसांगवी",
            "पिंपरी अंबा",
            "पेठ",
            "बाभळगाव",
            "बींदगीहळ",
            "बोकनगाव",
            "बोरगाव बु.",
            "भुईसमुद्रगा",
            "मुरूड बु.",
            "मुशिराबाद",
            "येळी",
            "रामेश्वर",
            "रायवाडी",
            "वासनगाव",
            "शिराळा",
            "शेलू बु.",
            "सळगरा खु.",
            "सळगरा बु.",
            "सिकंदरपूर",
            "सिर्सी",
            "हिसोरी",
            "अजनी बु.",
            "आनंदवाडी",
            "आरी",
            "कळमगाव",
            "गणेशवाडी",
            "घुगी (सांगवी)",
            "तुरुकवाडी",
            "थेरगाव",
            "फक्रानपूर",
            "भिंगोळी",
            "येरोळ",
            "रापका",
            "लक्कडजवळगा",
            "वंजारखेडा",
            "शिवपूर",
            "साकोळ",
            "सावरगाव",
            "सांगवी (घुगी)",
            "हळकी",
            "हिप्पळगाव",
            "अंबापूर",
            "गौरखेडा",
            "चोर अंबा",
            "दह्यापूर",
            "दिघी",
            "नागापूर",
            "पांजरा बोथाळी",
            "बोदड",
            "मळतपूर",
            "रोहाना",
            "लक्ष्मीपूर",
            "वाई",
            "साईखेडा",
            "साळदरा",
            "सिरपूर",
            "हिवरा",
            "झडगाव",
            "ठेकाकिन्ही",
            "तुम्नी",
            "धाडी",
            "पोरगव्हाण",
            "पंचाळा",
            "पंढुर्णा",
            "बोरखेडी",
            "बोरगाव",
            "मिळनपूर",
            "रंभापूर",
            "शहापूर",
            "सहूर",
            "सातनूर",
            "अंभोरा",
            "एनी दोडका",
            "खैरवाडा",
            "पंजारे गोंडी",
            "बांगडपूर",
            "बुधाळगड",
            "महदपूर",
            "मारकसूर",
            "येल्हती",
            "रायपूर",
            "सिंदी विहीरी",
            "अकोली",
            "अळोदा",
            "करमळापूर",
            "काशिमपूर",
            "कोल्हापूर",
            "खर्डा",
            "गणेशपूर",
            "चंद्रपूर",
            "जैतापूर",
            "तळनी (खंडेराव)",
            "दुरगडा",
            "बाबापूर",
            "बोपापूर",
            "भिडी",
            "भोजनखेडा",
            "महमदपूर",
            "मुंड",
            "मोमीनपूर",
            "मोहनापूर",
            "वाबगाव",
            "सिरपूर (होरे)",
            "सेकापूर",
            "सैदापूर",
            "हुर्दानपूर",
            "हुस्नापूर",
            "अमिनपूर",
            "अंबापूर",
            "केळापूर",
            "टाहरपूर",
            "डोरली",
            "दापोरी",
            "देगाव",
            "देवनगाव",
            "नेरी",
            "भावनपूर",
            "भिवापूर",
            "भुईगाव",
            "रामपूर",
            "लोनसावळी",
            "वल्हापूर",
            "वाईफड",
            "सेलूकाटे",
            "इसाबपूर",
            "तावी",
            "नांदपूर",
            "परसोडा",
            "पार्डी",
            "पेठ",
            "फरीदपूर",
            "मोहगाव",
            "रज्जाकपूर",
            "वाईगाव",
            "वांधळी",
            "अळगाव",
            "अंजनगाव",
            "उत्तमपूर",
            "किन्हाळा",
            "खाप्री (ढोने)",
            "चिंचोली",
            "जुनोना",
            "तुळजापूर",
            "दबलपूर",
            "दहेगाव (गोसाई)",
            "धापकी",
            "पहेलानपूर",
            "बाखळापूर",
            "बोंडसुळा",
            "शिवंणगांव",
            "हमदापूर",
            "कान्होळी",
            "कासापूर",
            "खोलापूर",
            "गडेगाव",
            "गंगापूर",
            "चांकी",
            "टेंभा",
            "दोंदुडा",
            "नांदगाव",
            "बळापूर",
            "बामबर्डा",
            "मनसवोळी",
            "मेंडुकडोह",
            "रोहनखेडा",
            "वडनेर",
            "अलीमर्दापूर",
            "इसाबपूर",
            "उमर्डा (बाजार)",
            "कार्ली",
            "किनखेड",
            "किसन नगर",
            "गणेशपूर",
            "गैवळ",
            "झोडगा",
            "डोनद बु.",
            "ढांज खु.",
            "दुधोरा",
            "देवचांदी",
            "धानोरा ताथोड",
            "धामनी",
            "नारेगाव",
            "पिलखेडा",
            "पिंपरी वरघाट",
            "ब्राम्हणवाडा",
            "मनभा",
            "मांडवा",
            "मोरपूर",
            "मोहगव्हाण",
            "यावर्डी",
            "येवता",
            "लोहारा",
            "वडगाव",
            "वडगाव  (रांगे)",
            "वाई प्र. करंजा",
            "वाघोला",
            "वाढवी",
            "शेलू बु.",
            "सोहळ",
            "अजनी",
            "अमदरी",
            "असोळा खु.",
            "उज्वल नगर",
            "उमरी खु.",
            "काकड चिखली",
            "कारखेडा",
            "कारपा",
            "गळमगाव",
            "गव्हा",
            "गारटेक",
            "गोगजाई",
            "गोस्ता",
            "चाकुर",
            "चौसाळा",
            "जमुना खु.",
            "ढावंडा",
            "दहीठाणा",
            "दारा",
            "नइन्य",
            "पाळोदी",
            "पिंपळ शेंडा",
            "पोहरादेवी",
            "फुळूमरी",
            "बीडगाव",
            "भांडेगाव",
            "भुळी",
            "भोयनी",
            "मेंद्रा",
            "म्हसनी",
            "रतनवाडी",
            "राजीतनगर",
            "रीक्त",
            "वसंतनगर (नविन)",
            "वाटफळ",
            "विठोळी",
            "सावळी",
            "हतोळी",
            "उदी",
            "उमरदरी",
            "कळकमाथा",
            "कावरदरी",
            "किन्हीराजा",
            "कुतरडोह",
            "गुंजा",
            "जौळका",
            "धामधानी",
            "पिंपळवाडी",
            "पिंपळ शेंडा",
            "प्रांग्राबंडी",
            "माळेगाव नाईक किन्ही",
            "वाडी रामराव",
            "वारदरी खु.",
            "वारदरी बु.",
            "सोनखस",
            "ईस्माइलपूर",
            "एकंबा",
            "कवथाळ",
            "कुंभी",
            "खाडी",
            "गिंभा",
            "घोटा",
            "चाकवा",
            "चुकंबा",
            "जनुना बु.",
            "झाडगाव",
            "डवखा",
            "दिलावळपूर",
            "धोत्रा",
            "पोटी",
            "बेलखेड",
            "बोरव्हा बु.",
            "मोहरी",
            "लाही",
            "वसंतवाडी",
            "शहापूर बु.",
            "सनळापूर",
            "अगरवाडी",
            "असोळा",
            "एकलासपूर",
            "कान्हेरी",
            "कु-हा",
            "कंकरवाडी",
            "चाकोळी",
            "नेर",
            "पिंपरखेड",
            "बोरखेडी",
            "भार जहागीर",
            "मोप",
            "मोरगव्हाण",
            "मोहजाबंडी",
            "लोणी खु.",
            "लोणी बु.",
            "शेलू खडसे",
            "सरापखेड",
            "असोळा",
            "कळंब महली",
            "काजळंबा",
            "कामथवाडा",
            "कार्ली",
            "किनखेडा",
            "खरोळा",
            "गोंदेगाव",
            "जांभरुन महली",
            "तोर्नाळा",
            "देपूल",
            "पांडव उमरा",
            "फुलसाक्रा",
            "भटुम्रा",
            "माळेगाव एन.भट उमरा",
            "वाघजळी",
            "वारा जहांगीर",
            "साक्रा",
            "सावंगा जहागीर",
            "सुरळा",
            "रीक्त",
            "रीक्त",
            "रीक्त",
            "रीक्त",
            "रीक्त",


    };

    String rabi_crop_types[] = {
            "-----",
            "रबी मिरची",
            "रबी फुल कोबी",
            "रबी भेंडी",
            "रबी मका",
            "रबी भुईमुग",
            "रबी बटाटा",
            "रबी भाजीपाला",
            "रबी गहू",
            "रबी ज्वारी",
            "रबी सुर्यफुल",
            "रबी वांगे",
            "रबी कांदा",
            "रबी टोमेटो",
            "रबी हरभरा",
            "रबी चारा पिके",
    };

    String kharif_crop_types[] = {
            "-----",
            "बाजरी",
            "सोयाबीन",
            "कापूस",
            "हळद",
            "उडिद",
            "तूर",
            "मुग",
            "खरीप फुल कोबी",
            "खरीप मिरची",
            "खरीप भाजीपाला (भेंडी, गावर, कारली ई.)",
            "खरीप कांदा",
            "खरीप टोमेटो",
            "खरीप चारा पिके",
            "खरीप ज्वारी",
            "खरीप सुर्यफुल",
            "खरीप मका",
            "खरीप वांगी",
            "खरीप भात",
            "खरीप भुईमुग",

    };
    String other_crop_types[] = {
            "-----",
            "केळी",
            "लिंबू",
            "द्राक्ष",
            "बटाटा",
            "मोसंबी",
            "कमी कालावधीचा भाजीपाला (मुळा,मेथी ई.)",
            "ऊस",
            "डाळिंब",
            "संत्रा",
            "आंबा",
            "चिकू",
            "सिताफळ",
            "ड्रॅगनफळ"

    };
    String crop_types[] = {
            /* "मुख्य पीक निवडा",*/
            "-----",
            "रबी मिरची",
            "गायरान",
            "केळी",
            "खरीप फुल कोबी",
            "रबी फुल कोबी",
            "खरीप मिरची",
            "कायम पड (गावठाणसह)",
            "रबी भेंडी",
            "रबी मका",
            "चालु पड",
            "रबी भुईमुग",
            "खरीप भाजीपाला (भेंडी, गावर, कारली ई.)",
            "रबी बटाटा",
            "रबी भाजीपाला",
            "रबी गहू",
            "खरीप कांदा",
            "कापूस",
            "खरीप टोमेटो",
            "खरीप चारा पिके",
            "हळद",
            "खरीप ज्वारी",
            "रबी ज्वारी",
            "खरीप सुर्यफुल",
            "उडिद",
            "खरीप मका",
            "तूर",
            "बिगर शेती",
            "सोयाबीन",
            "खरीप वांगी",
            "लिंबू",
            "रबी सुर्यफुल",
            "द्राक्ष",
            "मुग",
            "खरीप भात",
            "पोटखराबा",
            "बटाटा",
            "मोसंबी",
            "रबी वांगे",
            "कमी कालावधीचा भाजीपाला (मुळा,मेथी ई.)",
            "ऊस",
            "वनक्षेत्र",
            "डाळिंब",
            "रबी टोमेटो",
            "रबी हरभरा",
            "संत्रा",
            "रबी चारा पिके",
            "खरीप भुईमुग",
            "बाजरी",
            "रबी कांदा",


    };

    String Landuse_types[] = {

            /*"जमिन वापर निवडा",*/
            "-----",
            "शेती",
            "वन क्षेत्र",
            "पोटखराबा",
            "गायरान",
            "कायम पड (गावठाण सह)",
            "चालु पड"

    };


    List<Data> dataArray;
    List<Bitmap> imgArray;
    Bitmap img1;
    String img1_name = "";
    Bitmap img2;
    String img2_name = "";
    public static final int PICKFILE_RESULT_CODE = 1;
    private int PICK_IMAGE_REQUEST = 1, PICK_PROFILE_IMAGE_REQUEST = 1;
    private Button btnChooseFile, btnChooseFile1;
    private TextView tvItemPath;

    private Uri fileUri;
    private String filePath;
    private Bitmap bitmap;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    ImageView imViewAndroid, imViewAndroid1;
    HashMap<Integer, String> img_to_string = new HashMap<>();
    int intmap = 0;
    private String location = "";
    private String farmername= "", filename="";
    String fullname="",mobile_num="", census_code="";
    String[] census_code_arr;
    boolean census_found=true;
    HashMap<String, String> village_to_census_map = new HashMap<>();

    String clicked="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soil__info);
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            fullname=extras.getString("fullname");
            mobile_num=extras.getString("mobile");
//            census_code=extras.getString("census_code");
            census_code_arr = extras.getStringArray("census_code");
        }

//        fetchFarmerNames(census_code);

        btnChooseFile = (Button) findViewById(R.id.btn_choose_file);
        imViewAndroid = findViewById(R.id.imViewAndroid);
        imViewAndroid.setVisibility(View.GONE);
        btnChooseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Farmer_name_1.getVisibility()==View.VISIBLE && Farmer_name_1.getText().toString().isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया शेतकरी नाव जोडा ", Toast.LENGTH_LONG).show();
                }
                else if(Farmer_name.getSelectedItem().toString().isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया शेतकरी नाव जोडा ", Toast.LENGTH_LONG).show();
                }else if(Contact_no.getText().toString().isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया मोबाईल नंबर जोडा ", Toast.LENGTH_LONG).show();
                }else if (location.isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया स्थान जोडा ", Toast.LENGTH_LONG).show();
                }else{
                    location = Lat.getText().toString()+", "+Longit.getText().toString();

                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    clicked="btn1";
                    startActivityForResult(i, 100);
                }

            }
        });

        btnChooseFile1 = (Button) findViewById(R.id.btn_choose_file1);
        imViewAndroid1 = findViewById(R.id.imViewAndroid1);
        imViewAndroid1.setVisibility(View.GONE);
        btnChooseFile1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Farmer_name_1.getVisibility()==View.VISIBLE && Farmer_name_1.getText().toString().isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया शेतकरी नाव जोडा ", Toast.LENGTH_LONG).show();
                }
                else if(Farmer_name.getSelectedItem().toString().isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया शेतकरी नाव जोडा ", Toast.LENGTH_LONG).show();
                }else if(Contact_no.getText().toString().isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया मोबाईल नंबर जोडा ", Toast.LENGTH_LONG).show();
                }else if (location.isEmpty()){
                    Toast.makeText(Soil_Info.this, "कृपया स्थान जोडा ", Toast.LENGTH_LONG).show();
                }else{
                    location = Lat.getText().toString()+", "+Longit.getText().toString();

                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    clicked="btn2";
                    startActivityForResult(i, 100);
                }

            }
        });


        dataArray = new ArrayList<Data>();
        imgArray = new ArrayList<Bitmap>();


        getSupportActionBar().setTitle("माती सर्वेक्षण");

        TimeStmp = (EditText) findViewById(R.id.Timestamp);
        Lat = (EditText) findViewById(R.id.Latitude);
        Longit = (EditText) findViewById(R.id.Longitude);
//        Farmer_name = (EditText) findViewById(R.id.Farmer_name);
        Farmer_name = findViewById(R.id.Farmer_name_1);
        final ArrayAdapter<String> arrayAdapter_farmer = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, farmer_names);
        Farmer_name.setAdapter(arrayAdapter_farmer);

        Farmer_name_1 = findViewById(R.id.Farmer_name_text);
        Farmer_name_1.setVisibility(View.GONE);


//        Farmer_name.setText(fullname);
        Contact_no = (EditText) findViewById(R.id.Contact_number);
//        Contact_no.setText(mobile_num);
        Gat_no = (EditText) findViewById(R.id.Gat_no);
        Soil_depth = (EditText) findViewById(R.id.Soil_depth);
        gpsbutton = (Button) findViewById(R.id.GPSbutton);

        submitbutton = (Button) findViewById(R.id.Submit);
        savebutton = (Button) findViewById(R.id.Save);

        File csvFILE_ = new File(this.getExternalFilesDir(null), "Final.csv");
        if(csvFILE_.exists()){
            submitbutton.setVisibility(View.VISIBLE);
        }else{
            submitbutton.setVisibility(View.GONE);
        }

        yield = (EditText) findViewById(R.id.Yield_kharif);
        yield_rabi = findViewById(R.id.Yield_rabi);
        yield_other = findViewById(R.id.Yield_other);
        progress = findViewById(R.id.progressBar);
        progress1 = findViewById(R.id.progressBar1);

        //watering=(EditText)findViewById(R.id.Watering);

//        setTimestamp();
        builder = new AlertDialog.Builder(Soil_Info.this);


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        geocoder = new Geocoder(this, Locale.getDefault());
        mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        medit = mPref.edit();

        Resources res = getResources();
        InputStream in_s = res.openRawResource(R.raw.villages5000);
        BufferedReader br = new BufferedReader(new InputStreamReader(in_s));

        final Helper helper = new Helper(br);
//        tal_in_dist = helper.getTalukasByDist(districts[0]);
//        vil_in_tal = helper.getVillagesByTal(talukas[0]);
        HashMap<String, String[]> census_code_data = helper.census_code_map;
//        census_code="6464";
        try{
            HashMap<String,String>  dist = new HashMap<>();
            HashMap<String,String>  tal = new HashMap<>();
            HashMap<String,String>  vil = new HashMap<>();

            for(String census_code:census_code_arr){
                String data_for_census_code[] = census_code_data.get(census_code);
                dist.put(data_for_census_code[0],data_for_census_code[0]);
                tal.put(data_for_census_code[1], data_for_census_code[1]);
                vil.put(data_for_census_code[2], data_for_census_code[2]);
                village_to_census_map.put(data_for_census_code[2],census_code);
            }
//            districts = new String[]{data_for_census_code[0]};
//            tal_in_dist = new String[]{data_for_census_code[1]};
//            vil_in_tal = new String[]{data_for_census_code[2]};
            districts = dist.keySet().toArray(new String[dist.size()]);
            tal_in_dist= tal.keySet().toArray(new String[tal.size()]);
            vil_in_tal = vil.keySet().toArray(new String[vil.size()]);

        }catch (Exception e){
            tal_in_dist = helper.getTalukasByDist(districts[0]);
            vil_in_tal = helper.getVillagesByTal(talukas[0]);
            census_found=false;
        }


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, districts);
        district = findViewById(R.id.district_name);
        district.setAdapter(arrayAdapter);
//        arrayAdapter.clear();
//        arrayAdapter.addAll(tal_in_dist);
//        arrayAdapter.notifyDataSetChanged();


        ArrayList<String> tal_lst = new ArrayList<String>(Arrays.asList(tal_in_dist));
        final ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, tal_lst);
        taluka = findViewById(R.id.taluka_name);
        taluka.setAdapter(arrayAdapter1);

        ArrayList<String> vill_lst = new ArrayList<String>(Arrays.asList(vil_in_tal));
        final ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, vill_lst);
        village = findViewById(R.id.village_name);
        village.setAdapter(arrayAdapter2);
        village.setSelection(0);


        district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String selected_dist = district.getSelectedItem().toString();

                Log.d("Selected district : ", selected_dist);
                if(!census_found)
                    tal_in_dist = helper.getTalukasByDist(selected_dist);

//                Log.d("Soil info ", "==================" + tal_in_dist.length);


                arrayAdapter1.clear();
                arrayAdapter1.addAll(tal_in_dist);
                arrayAdapter1.notifyDataSetChanged();
                final ArrayAdapter<String> arrayAdapter3_1 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, tal_in_dist);
                taluka.setAdapter(arrayAdapter3_1);
                taluka.setSelection(0);
                arrayAdapter2.clear();
                arrayAdapter2.notifyDataSetChanged();
                village.setSelection(0);


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
//                Log.d("Great", "Got it !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        });


//
//        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, tal_in_dist);
//        taluka = findViewById(R.id.taluka_name);
//        taluka.setAdapter(arrayAdapter1);
//        arrayAdapter1.notifyDataSetChanged();
//        arrayAdapter1.notifyDataSetChanged();


        taluka.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                String selected_tal = taluka.getSelectedItem().toString();

                Log.d("Selected taluka : ", selected_tal);
                if(!census_found)
                    vil_in_tal = helper.getVillagesByTal(selected_tal);


                arrayAdapter2.clear();
                arrayAdapter2.addAll(vil_in_tal);
                arrayAdapter2.notifyDataSetChanged();
                village.setSelection(0);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
//                Log.d("Great", "Got it !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }


        });

        village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected_village = village.getSelectedItem().toString();
                String census_code_="";
                try{
                    census_code_ = village_to_census_map.get(selected_village);
                }catch (Exception e){
                    census_code_ = "123456789";
                }
                Log.d("census code", census_code_);
                fetchFarmerNames(census_code_);
                final ArrayAdapter<String> arrayAdapter_farmer = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, farmer_names);
                Farmer_name.setAdapter(arrayAdapter_farmer);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//
//        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, vil_in_tal);
//        village = findViewById(R.id.village_name);
//        village.setAdapter(arrayAdapter2);


//        village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1,
//                                       int arg2, long arg3) {
//                // TODO Auto-generated method stub
//                String selected_tal = village.getSelectedItem().toString();
//
//                Log.d("Selected village : ",selected_tal);
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//                Log.d("Great", "Got it !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//            }
//        });
//
//        arrayAdapter2.notifyDataSetChanged();

        ArrayAdapter<String> arrayAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, soil_types);
        soil_type = findViewById(R.id.Soil_type);
        soil_type.setAdapter(arrayAdapter3);


        final ArrayAdapter<String> arrayAdapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, kharif_crop_types);
        final ArrayAdapter<String> arrayAdapter4_1 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, rabi_crop_types);
        final ArrayAdapter<String> arrayAdapter4_2 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, other_crop_types);

        crop_type = findViewById(R.id.Crop_name);
        crop_type.setAdapter(arrayAdapter4);
        crop_type_rabi = findViewById(R.id.Crop_name1);
        crop_type_rabi.setAdapter(arrayAdapter4_1);
        crop_type_other = findViewById(R.id.Crop_name2);
        crop_type_other.setAdapter(arrayAdapter4_2);

        ArrayAdapter<String> arrayAdapter6 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, years);
        year = findViewById(R.id.Year);
        year.setAdapter(arrayAdapter6);

//        ArrayAdapter<String> arrayAdapter7 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, watering_types);
//        watering_type = findViewById(R.id.Watering_type);
//        watering_type.setAdapter(arrayAdapter7);

        ArrayAdapter<String> arrayAdapter7_kharif = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, watering_types);
        watering_type_kharif = findViewById(R.id.Watering_type_kharif);
        watering_type_kharif.setAdapter(arrayAdapter7_kharif);

        ArrayAdapter<String> arrayAdapter7_rabi = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, watering_types);
        watering_type_rabi = findViewById(R.id.Watering_type_rabi);
        watering_type_rabi.setAdapter(arrayAdapter7_rabi);

        ArrayAdapter<String> arrayAdapter7_other = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, watering_types);
        watering_type_other = findViewById(R.id.Watering_type_other);
        watering_type_other.setAdapter(arrayAdapter7_other);


//        ArrayAdapter<String> arrayAdapter8 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, waterings);
//        watering = findViewById(R.id.Watering);
//        watering.setAdapter(arrayAdapter8);

        ArrayAdapter<String> arrayAdapter8_kharif = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, waterings);
        watering_kharif = findViewById(R.id.Watering_kharif);
        watering_kharif.setAdapter(arrayAdapter8_kharif);

        ArrayAdapter<String> arrayAdapter8_rabi = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, waterings);
        watering_rabi = findViewById(R.id.Watering_rabi);
        watering_rabi.setAdapter(arrayAdapter8_rabi);

        ArrayAdapter<String> arrayAdapter8_other = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, waterings);
        watering_other = findViewById(R.id.Watering_other);
        watering_other.setAdapter(arrayAdapter8_other);


        ArrayAdapter<String> arrayAdapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, Landuse_types);
        landuse_type = findViewById(R.id.Landuse);
        landuse_type.setAdapter(arrayAdapter5);
        landuse_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int item = landuse_type.getSelectedItemPosition();
                if (item != 1) {
                    LinearLayout cropll = findViewById(R.id.cropt_type_ll);
                    crop_type.setSelection(0);
                    cropll.setVisibility(View.GONE);
                    LinearLayout cropll1 = findViewById(R.id.cropt_type_ll1);
                    cropll1.setVisibility(View.GONE);
                    crop_type_rabi.setSelection(0);
                    LinearLayout cropll2 = findViewById(R.id.cropt_type_ll2);
                    cropll2.setVisibility(View.GONE);
                    crop_type_other.setSelection(0);
                    LinearLayout peekpani = findViewById(R.id.peek_pani_ll);
                    year.setSelection(0);
                    watering_type_kharif.setSelection(0);
                    watering_type_rabi.setSelection(0);
                    watering_type_other.setSelection(0);
                    yield.setText("0");
                    yield_rabi.setText("0");
                    yield_other.setText("0");
                    watering_kharif.setSelection(0);
                    watering_rabi.setSelection(0);
                    watering_other.setSelection(0);

                    peekpani.setVisibility(View.GONE);

                } else {
                    LinearLayout cropll = findViewById(R.id.cropt_type_ll);
                    cropll.setVisibility(View.VISIBLE);
                    LinearLayout cropll1 = findViewById(R.id.cropt_type_ll1);
                    cropll1.setVisibility(View.VISIBLE);
                    LinearLayout cropll2 = findViewById(R.id.cropt_type_ll2);
                    cropll2.setVisibility(View.VISIBLE);
                    LinearLayout peekpani = findViewById(R.id.peek_pani_ll);
                    peekpani.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


//        setfields();
        gpsbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boolean_permission) {
                    setTimestamp();

                    medit.putString("service", "").commit();
                    if (mPref.getString("service", "").matches("")) {
                        medit.putString("service", "service").commit();

                        Intent intent = new Intent(getApplicationContext(), GoogleService.class);
                        startService(intent);

                    } else {
                        Toast.makeText(getApplicationContext(), "सेवा आधीपासून चालू आहे", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "कृपया जीपीएस सक्षम करा", Toast.LENGTH_SHORT).show();
                }

            }
        });

        fn_permission();

        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean fieldsOK;
                if(Farmer_name_1.getVisibility()== View.GONE)
                    fieldsOK = validate_fields(new EditText[]{Lat, Longit, TimeStmp, Contact_no, Gat_no, Soil_depth}, new MaterialSpinner[]{district, taluka, village, Farmer_name, soil_type, landuse_type});
                else
                    fieldsOK = validate_fields(new EditText[]{Lat, Longit, TimeStmp, Contact_no, Gat_no, Soil_depth, Farmer_name_1}, new MaterialSpinner[]{district, taluka, village, soil_type, landuse_type});
                Log.d("fieldsok 5964", String.valueOf(fieldsOK));
                //TODO: Validate watering
                int item = landuse_type.getSelectedItemPosition();
                if(item==1){
                    int kharif_select = crop_type.getSelectedItemPosition();
                    int rabi_select = crop_type_rabi.getSelectedItemPosition();
                    int other_select = crop_type_other.getSelectedItemPosition();
                    int watering_type_kharif_select = watering_type_kharif.getSelectedItemPosition();
                    int watering_type_rabi_select = watering_type_rabi.getSelectedItemPosition();
                    int watering_type_other_select = watering_type_other.getSelectedItemPosition();
                    if(kharif_select==0 && rabi_select==0 && other_select==0) {
                        fieldsOK = false;
                        Log.d("fieldsok 5976", String.valueOf(fieldsOK));
                    }
                    String kharifyield = yield.getText().toString();
                    String rabiyield = yield_rabi.getText().toString();
                    String otheryield = yield_other.getText().toString();
                    if(kharif_select!=0 && (kharifyield.trim().isEmpty() || watering_type_kharif_select==0)){
                        fieldsOK=false;
                        Log.d("fieldsok 5983", String.valueOf(fieldsOK));
                    }
                    if(rabi_select!=0 && (rabiyield.trim().isEmpty() || watering_type_rabi_select==0)){
                        fieldsOK=false;
                        Log.d("fieldsok 5987", String.valueOf(fieldsOK));
                    }
                    if(other_select!=0 && (otheryield.trim().isEmpty() || watering_type_other_select==0)){
                        fieldsOK=false;
                        Log.d("fieldsok 5991", String.valueOf(fieldsOK));
                    }
                    int yr = year.getSelectedItemPosition();
                    if(yr==0){
//                        fieldsOK = false;
//                        Log.d("fieldsok 5996", String.valueOf(fieldsOK));
                    }

                }
                if (Contact_no.getText().toString().length() != 10) {
                    fieldsOK = false;
                    Toast.makeText(Soil_Info.this, "कृपया दहा अंकी नंबर जोडा", Toast.LENGTH_SHORT).show();
                } else if (Farmer_name_1.getVisibility() == View.VISIBLE && Pattern.compile("[0-9]").matcher(Farmer_name_1.getText().toString()).find()) {
                    fieldsOK = false;
                    Toast.makeText(Soil_Info.this, "शेतकऱ्याचे नावात अंक प्रतिबंधित", Toast.LENGTH_SHORT).show();
                }else if(img1 == null){
                    Toast.makeText(Soil_Info.this, "छायाचित्र १ जोडा", Toast.LENGTH_LONG).show();
                }else if (img2 == null){
                    Toast.makeText(Soil_Info.this, "छायाचित्र 2 जोडा", Toast.LENGTH_LONG).show();
                }

                else if (fieldsOK) {

//                    Toast.makeText(Soil_Info.this, "यशस्वीरित्या डेटा प्रविष्ट केला", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedpreferences.edit();


//                    Toast.makeText(Soil_Info.this, " "+ district.getSelectedItemPosition(), Toast.LENGTH_SHORT).show();
                    editor.putInt(SelDistrict, district.getSelectedItemPosition());
                    editor.putInt(SelTaluka, taluka.getSelectedItemPosition());
                    editor.putInt(SelVillage, village.getSelectedItemPosition());
                    editor.putInt(SelSoil, soil_type.getSelectedItemPosition());
                    editor.apply();


                    try {
                        Log.d("img1 name before", img1_name);
                        uploadBitmap(img1, img1_name,"btn1", (Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString()), Contact_no.getText().toString());
                        uploadBitmap(img2, img2_name,"btn2", (Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString()), Contact_no.getText().toString());

                        Log.d("img1 name after", img1_name);
                        appendLog();
                        img1=null;
                        img2=null;
                        img1_name= "";
                        img2_name = "";
                        boolean fieldsclear = clear_fields(new EditText[]{Lat, Longit, TimeStmp, Contact_no, Gat_no, Soil_depth, Farmer_name_1}, new MaterialSpinner[]{district, taluka, village, Farmer_name, soil_type, crop_type, crop_type_rabi, crop_type_other, landuse_type, year, watering_kharif, watering_rabi, watering_other, watering_type_kharif, watering_type_rabi, watering_type_other});
                        yield.setText("0");
                        yield_rabi.setText("0");
                        yield_other.setText("0");
                        Toast.makeText(getApplicationContext(), "Data Saved Locally!!",Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(Soil_Info.this, "कृपया सर्व तपशील प्रविष्ट करा", Toast.LENGTH_SHORT).show();
                }
                File csvFILE_ = new File(getApplicationContext().getExternalFilesDir(null), "Final.csv");
                if(csvFILE_.exists()){
                    submitbutton.setVisibility(View.VISIBLE);
                }else{
                    submitbutton.setVisibility(View.GONE);
                }



            }
        });


        soil_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                int item = soil_type.getSelectedItemPosition();

//                if (item == 1) {
//                    Toast.makeText(Soil_Info.this, "या मातीत पाणी त्वरीत वाहते", Toast.LENGTH_SHORT).show();
//                } else if (item == 2) {
//                    Toast.makeText(Soil_Info.this, "या मातीत पाणी मध्यवर्ती वेगाने वाहते", Toast.LENGTH_SHORT).show();
//                } else if (item == 3) {
//                    Toast.makeText(Soil_Info.this, "या मातीत पाणी अगदी कमी वेगाने वाहते", Toast.LENGTH_SHORT).show();
//                }


            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOnline() == false) {
                    Toast.makeText(Soil_Info.this, "कृपया इंटरनेट कनेक्शन चालू असल्याची बतावणी करा", Toast.LENGTH_SHORT).show();
                } else {
                    // boolean fieldsOK = validate_fields(new EditText[] { Lat,Longit,TimeStmp,Farmer_name,Contact_no,Gat_no,Soil_depth });

//                if(fieldsOK)
//                {
                    apicall_data();
                    boolean fieldsclear = clear_fields(new EditText[]{Lat, Longit, TimeStmp, Contact_no, Gat_no, Soil_depth, yield, yield_rabi, yield_other}, new MaterialSpinner[]{district, taluka, village, Farmer_name, soil_type, crop_type, crop_type_rabi, crop_type_other, landuse_type, year, watering_type_kharif, watering_type_rabi, watering_type_other, watering_kharif, watering_rabi, watering_other});
//                }
//                else
//                {
//                    Toast.makeText(Soil_Info.this, "कृपया सर्व तपशील प्रविष्ट करा", Toast.LENGTH_SHORT).show();
//                }


                }

                File csvFILE_ = new File(getApplicationContext().getExternalFilesDir(null), "Final.csv");
                if(csvFILE_.exists()){
                    submitbutton.setVisibility(View.VISIBLE);
                }else{
                    submitbutton.setVisibility(View.GONE);
                }


            }
        });
    }

    private void get_csv_data() {

        String path = String.valueOf(this.getExternalFilesDir(null));
        File csvFILE = new File(this.getExternalFilesDir(null), "Final.csv");

        try {
            CSVReader csvREAD = new CSVReader(new
                    FileReader(csvFILE.getAbsolutePath()));
            String[] csvLINE;
            int skip = 0;

            while ((csvLINE = csvREAD.readNext()) != null) {
                if (skip > 0)//becasue first line is column headers
                {
                    String PARAM_DISTRICT = csvLINE[0];
                    String PARAM_TALUKA = csvLINE[1];
                    String PARAM_VILLAGE = csvLINE[2];
                    String PARAM_FARMER_NAME = csvLINE[3];
                    String PARAM_CONTACT_NUMBER = csvLINE[4];
                    String PARAM_GAT_NUMBER = csvLINE[5];
                    String PARAM_CROP_NAME_KHARIF = csvLINE[6];
                    String PARAM_CROP_NAME_RABI = csvLINE[7];
                    String PARAM_CROP_NAME_OTHER = csvLINE[8];
                    String PARAM_LANDUSE = csvLINE[9];
                    String PARAM_SOIL_TYPE = csvLINE[10];
                    double PARAM_SOIL_DEPTH = Double.parseDouble(csvLINE[11]);
                    double PARAM_LATITUDE = Double.parseDouble(csvLINE[12]);
                    double PARAM_LONGITUDE = Double.parseDouble(csvLINE[13]);
                    String PARAM_TIMESTAMP = csvLINE[14];
                    String PARAM_YEAR = csvLINE[15];
                    double PARAM_YIELD=0.0;
                    double PARAM_YIELD_RABI=0.0;
                    double PARAM_YIELD_OTHER=0.0;
                    try {
                        PARAM_YIELD= Double.parseDouble(csvLINE[16]);
                    }catch (Exception e){
                        PARAM_YIELD = 0.0;
                    }
                    try{
                        PARAM_YIELD_RABI = Double.parseDouble(csvLINE[17]);
                    }catch (Exception e){
                        PARAM_YIELD_RABI=0.0;
                    }
//                    double PARAM_YIELD_RABI = Double.parseDouble(csvLINE[17]);
                    try{
                        PARAM_YIELD_OTHER = Double.parseDouble(csvLINE[18]);
                    }catch (Exception e){
                        PARAM_YIELD_OTHER = 0.0;
                    }
//                    double PARAM_YIELD_OTHER = Double.parseDouble(csvLINE[18]);

                    int PARAM_WATERING_KHARIF = Integer.parseInt(csvLINE[19]);
                    int PARAM_WATERING_RABI = Integer.parseInt(csvLINE[20]);
                    int PARAM_WATERING_OTHER = Integer.parseInt(csvLINE[21]);
                    String PARAM_WATERING_TYPE_KHARIF = csvLINE[22];
                    String PARAM_WATERING_TYPE_RABI = csvLINE[23];
                    String PARAM_WATERING_TYPE_OTHER = csvLINE[24];
                    String IMG1_NAME = csvLINE[25];
                    String IMG2_NAME = csvLINE[26];
                    Data STUD_OBJECT = new Data(PARAM_DISTRICT, PARAM_TALUKA, PARAM_VILLAGE,
                            PARAM_FARMER_NAME, PARAM_CONTACT_NUMBER, PARAM_GAT_NUMBER,
                            PARAM_CROP_NAME_KHARIF, PARAM_CROP_NAME_RABI, PARAM_CROP_NAME_OTHER, PARAM_LANDUSE, PARAM_SOIL_TYPE, PARAM_YEAR, PARAM_WATERING_TYPE_KHARIF,PARAM_WATERING_TYPE_RABI,PARAM_WATERING_TYPE_OTHER,PARAM_SOIL_DEPTH,
                            PARAM_YIELD, PARAM_YIELD_RABI, PARAM_YIELD_OTHER, PARAM_WATERING_KHARIF, PARAM_WATERING_RABI, PARAM_WATERING_OTHER, PARAM_LATITUDE, PARAM_LONGITUDE, PARAM_TIMESTAMP, IMG1_NAME, IMG2_NAME,mobile_num,fullname
                    );
                    dataArray.add(STUD_OBJECT);
                } else {

                    skip++;
                }
            }

        } catch (FileNotFoundException e) {
            Toast.makeText(this, "फाइल सापडली नाही अपवाद", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            csvFILE.delete();
        }
    }


    private void apicall_data() {

        get_csv_data();
//        Data dt=new Data( "SS","PP","AA","HH","7709405270","52/A","R","Landuse","heavy",3,75.254568,19.2544,"323-4242");
//

        //add into List array
//        dataArray.add(dt);

        //list array to json string
        Gson gson = new Gson();
        if(dataArray.isEmpty()){
            return;
        }
        for(Data d: dataArray){
            List<Data> dataArray_ = new ArrayList<Data>();
            dataArray_.add(d);
            final String newDataArray = gson.toJson(dataArray_); //dataarray is list array
            Log.d("newdata", newDataArray.toString());
            //now we have json string lets send it to server
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    final String result = response.toString();
                    Log.d("response", "result " + result);
                    Toast.makeText(Soil_Info.this, result, Toast.LENGTH_SHORT).show();
                    intmap = 0;
                }
            }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(Soil_Info.this, "त्रुटी आली", Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    error.getMessage();
//                Log.d("Maps:", " Error: " + new String(error.networkResponse.data));

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("array", newDataArray);
                    return params;
                }
            };


            MySingleton.getInstance(Soil_Info.this).addTorequestque(stringRequest);

            stringRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {
//                    Toast.makeText(Soil_Info.this, "Error Persists Timeout", Toast.LENGTH_SHORT).show();
                }
            });

        }

        dataArray.clear();
        uploadPendingBitmap();
    }


    private boolean clear_fields(EditText[] fields, MaterialSpinner[] dd_fields) {

        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() > 0) {
                currentField.setText("");
            }
        }
        for (int i = 0; i < dd_fields.length; i++) {
            MaterialSpinner ms = dd_fields[i];
            ms.setSelection(0);
        }

//        if((district.getSelectedItemPosition()!=0) || (taluka.getSelectedItemPosition()==0) || (village.getSelectedItemPosition()==0) || (soil_type.getSelectedItemPosition()==0) || (crop_type.getSelectedItemPosition()==0) || (landuse_type.getSelectedItemPosition()==0)  )
//        {
//            return false;
//        }

        return true;

    }

//    private void apicall() {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                builder.setTitle("सर्व्हर प्रतिसाद");
//                builder.setMessage("प्रतिसाद: "+response);
//                builder.setPositiveButton("ठीक आहे", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        Toast.makeText(Soil_Info.this, "येथे फील्ड साफ करा ", Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//                AlertDialog alertDialog =builder.create();
//                alertDialog.show();
//                Toast.makeText(Soil_Info.this, "यशस्वी", Toast.LENGTH_SHORT).show();
//
//            }
//        }
//
//                , new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Toast.makeText(Soil_Info.this, "त्रुटी आली", Toast.LENGTH_SHORT).show();
//                error.printStackTrace();
//
//            }
//        }){
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                Map <String,String> params=new HashMap<>();
//
//                params.put("district_name", "मंजरुळ");
//                params.put("taluka_name", "मंजरुळ");
//                params.put("village_name", "मंजरुळ");
//                params.put("farmer_name", "Swapnil");
//                params.put("contact_no", "7709405270");
//                params.put("gat_no", "54/2");
//                params.put("crop_name", "मंजरुळ");
//                params.put("landuse", "मंजरुळ");
//                params.put("soil_type", "मंजरुळ");
//                params.put("soil_depth", "1.5");
//                params.put("latitude", "19.2545845584588");
//                params.put("longitude", "71.25458458");
//                params.put("time_info", "15-04-2019--09:38:44");
////                params.put("time_info", "shah");
//
//                return params;
//            }
//        };
//
//
//        MySingleton.getInstance(Soil_Info.this).addTorequestque(stringRequest);
//
//        stringRequest.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
////                    Toast.makeText(Soil_Info.this, "Error Persists Timeout", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void setfields() {
//        district.setSelection(2);
//        taluka.setSelection(3);
//        village.setSelection(4);
//        Farmer_name.setText("Swapnil");
//        Contact_no.setText("7709405270");
//        Gat_no.setText("54");
//        soil_type.setSelection(2);
//        Soil_depth.setText("3");
//        crop_type.setSelection(2);;
//        landuse_type.setSelection(2);
//        year.setSelection(2);
//        yield.setText("20");
//        watering.setSelection(1);
//        watering_type.setSelection(2);

        SharedPreferences prfs = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Integer SelDistrictID = prfs.getInt(SelDistrict, 0);
        Integer SelTalukaID = prfs.getInt(SelTaluka, 0);
        Integer SelVillageID = prfs.getInt(SelVillage, 0);
        Integer SelSoilID = prfs.getInt(SelSoil, 0);


        district.setSelection(SelDistrictID);
        taluka.setSelection(SelTalukaID);
        village.setSelection(SelVillageID);
        landuse_type.setSelection(1);
//        soil_type.setSelection(1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            if(clicked.equals("btn1")){
                //getting the image Uri
                Uri imageUri = data.getData();
                try {
                    //getting bitmap object from uri
                    img1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    //displaying selected image to imageview
                    imViewAndroid.setImageBitmap(img1);
                    imViewAndroid.setVisibility(View.VISIBLE);
                    //calling the method uploadBitmap to upload image
                    long imagename = System.currentTimeMillis();
                    img1_name = (Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString())+"_"+imagename+".jpg";
//                    uploadBitmap(img1, null);
                } catch (IOException e) {
                    e.printStackTrace();
                    img1_name="";
                }
            }else if(clicked.equals("btn2")){
                //getting the image Uri
                Uri imageUri = data.getData();
                try {
                    //getting bitmap object from uri
                    img2 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    //displaying selected image to imageview
                    imViewAndroid1.setImageBitmap(img2);
                    imViewAndroid1.setVisibility(View.VISIBLE);
                    //calling the method uploadBitmap to upload image
                    long imagename = System.currentTimeMillis();
                    img2_name = (Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString())+"_"+imagename+".jpg";
//                    uploadBitmap(img2, null);
                } catch (IOException e) {
                    e.printStackTrace();
                    img2_name="";
                }
            }

        }
    }
    public void saveLocally(Bitmap bitmap, String imgname, String name, String phone){
        verifyStoragePermissions(this);
        String path = Environment.getExternalStorageDirectory().toString()+"/.soilapp/";
        File f=new File(path);
        if(!f.exists()){
            if(!f.mkdirs()){
                Toast.makeText(getApplicationContext(), "Not able to Write", Toast.LENGTH_LONG).show();
            }
        }
        OutputStream fOut = null;
        Integer counter = 0;
        Log.d("imgname", imgname);
        File file = new File(path, imgname);
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, bitmap.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(bitmap, 0, 0, null);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setAlpha(100);
        paint.setTextSize(100);
        paint.setAntiAlias(true);
        paint.setUnderlineText(false);
        canvas.drawText("LatLong: "+location, 10, 100, paint);
        canvas.drawText("Name: "+name, 10,200, paint);
        canvas.drawText("Phone: "+phone, 10,300,paint);
        try {
            fOut = new FileOutputStream(file);
            result.compress(Bitmap.CompressFormat.JPEG, 40, fOut);
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream
            MediaStore.Images.Media.insertImage(getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }



    }
    public byte[] getFileDataFromDrawable(Bitmap bitmap, String name,String phone, boolean offline) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, bitmap.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(bitmap, 0, 0, null);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setAlpha(100);
        paint.setTextSize(90);
        paint.setAntiAlias(true);
        paint.setUnderlineText(false);
        if(!offline){
            canvas.drawText("LatLong: "+location, 10, 100, paint);
            if (!name.equals("n/a")) {
                canvas.drawText("Name: " + name, 10, 200, paint);
                canvas.drawText("Phone: " + phone, 10, 300, paint);
            }
        }
        result.compress(Bitmap.CompressFormat.JPEG, 35, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void uploadPendingBitmap(){
        checkPermissionREAD_EXTERNAL_STORAGE(getApplicationContext());
        String path = Environment.getExternalStorageDirectory().toString()+"/.soilapp/";
        File f=new File(path);
        if(!f.exists()){
            if(!f.mkdirs()){
                Toast.makeText(getApplicationContext(), "Not able to Write", Toast.LENGTH_LONG).show();
            }
        }

        File[] filesInFolder = f.listFiles();
        for (File file : filesInFolder) { //For each of the entries do:
            if (!file.isDirectory()) { //check that it's not a dir
                if(file.getName().contains(".jpg")){
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    filename = file.getName();
                    Log.d("file", filename);
                    uploadBitmap(bitmap, filename,"offline","n/a","n/a");
                    filename="";
                    file.delete();
                }
            }
        }

    }

    private void uploadBitmap(final Bitmap bitmap, final String filename, final String img, final String farmername, final String phone) {

        //getting the tag from the edittext
        final String URL ="http://gis.mahapocra.gov.in/soil_survey/img.php";
//        final String URL = "http://www.cse.iitb.ac.in/jaltarang/img.php";
        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Log.d("offlineimg", String.valueOf(response.statusCode));
                        Log.d("offlineimg", response.toString());
                        Toast.makeText(getApplicationContext(),"Image uploaded to Server", Toast.LENGTH_LONG).show();
                        if(img.equals("btn1"))
                            progress.setVisibility(View.GONE);
                        else if(img.equals("btn2"))
                            progress1.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        if(img.equals("btn1")){
                            progress.setVisibility(View.GONE);
                            Log.d("saving locally", filename);
                            Log.d("is log coming","log name");
                            saveLocally(bitmap,filename, farmername, phone);
                            Toast.makeText(getApplicationContext(), "Image Saved Locally!!", Toast.LENGTH_LONG).show();
                        }
                        else if(img.equals("btn2")) {
                            progress1.setVisibility(View.GONE);
                            Log.d("saving locally2", filename);
                            saveLocally(bitmap, filename, farmername, phone);
                            Toast.makeText(getApplicationContext(), "Image Saved Locally!!", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("tags", "tags");
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                double rand = Math.random();
                if(img.equals("offline")){
                    if(filename != null)
                        img1_name=filename;
                    Log.d("fname", (Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString()));
                    Log.d("img1 name", img1_name);
                    params.put("pic", new DataPart(img1_name, getFileDataFromDrawable(bitmap, farmername,phone, true)));
                }
                if(img.equals("btn1")){
//                    img1_name = Farmer_name.getText().toString()+"_"+imagename+".jpg";
                    if(filename != null)
                        img1_name=filename;
                    Log.d("fname", (Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString()));
                    Log.d("img1 name", img1_name);
                    params.put("pic", new DataPart(img1_name, getFileDataFromDrawable(bitmap, farmername,phone, false)));
                }
                if(img.equals("btn2")){
//                    img2_name = Farmer_name.getText().toString()+"_"+imagename+".jpg";
                    if(filename !=null)
                        img2_name=filename;
                    params.put("pic", new DataPart(img2_name, getFileDataFromDrawable(bitmap, farmername, phone, false)));
                }
                return params;
            }
        };

        //adding the request to volley
        if(img.equals("btn1"))
            progress.setVisibility(View.VISIBLE);
        else if(img.equals("btn2"))
            progress1.setVisibility(View.VISIBLE);

        Volley.newRequestQueue(this).add(volleyMultipartRequest);
        volleyMultipartRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {
//                    Toast.makeText(Soil_Info.this, "Error Persists Timeout", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void appendLog() throws IOException {
        ArrayList<String> list = new ArrayList<String>();
        list.add(district.getSelectedItem().toString());
        list.add(taluka.getSelectedItem().toString());
        list.add(village.getSelectedItem().toString());
        list.add(((Farmer_name_1.getVisibility() == View.VISIBLE ? Farmer_name_1.getText().toString(): Farmer_name.getSelectedItem().toString())));
        list.add(Contact_no.getText().toString());
        list.add(Gat_no.getText().toString());
        list.add(crop_type.getSelectedItem().toString());
        list.add(crop_type_rabi.getSelectedItem().toString());
        list.add(crop_type_other.getSelectedItem().toString());
        list.add(landuse_type.getSelectedItem().toString());
        list.add(soil_type.getSelectedItem().toString());
        list.add(Soil_depth.getText().toString());
        list.add(Lat.getText().toString());
        list.add(Longit.getText().toString());
        list.add(TimeStmp.getText().toString());
        list.add(year.getSelectedItem().toString());
        if(!yield.getText().toString().isEmpty())
            list.add(yield.getText().toString());
        else
            list.add("0");
        if(!yield_rabi.getText().toString().isEmpty())
            list.add(yield_rabi.getText().toString());
        else
            list.add("0");
        if(!yield_other.getText().toString().isEmpty())
            list.add(yield_other.getText().toString());
        else
            list.add("0");


        list.add(watering_kharif.getSelectedItem().toString());
        list.add(watering_rabi.getSelectedItem().toString());
        list.add(watering_other.getSelectedItem().toString());
        list.add(watering_type_kharif.getSelectedItem().toString());
        list.add(watering_type_rabi.getSelectedItem().toString());
        list.add(watering_type_other.getSelectedItem().toString());
        list.add(img1_name);
        list.add(img2_name);
        list.add(mobile_num);
        list.add(fullname);
        //  Log.d("apppendlog", String.valueOf(list));

        String data = "";

        for (String obj : list) {
            data = data + obj + ",";

        }
        appendLog(data);

        imViewAndroid.setImageDrawable(null);
        imViewAndroid1.setImageDrawable(null);
        imViewAndroid.setVisibility(View.GONE);
        imViewAndroid1.setVisibility(View.GONE);

    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }


    public void appendLog(String text) {


        String Testfile_name = "Final";
        Testfile_name = Testfile_name + ".csv";
        File logFile = new File(this.getExternalFilesDir(null), Testfile_name);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
                String header = "District,Taluka,Village,Farmer_name,Contact_Number,Gat_Number,Crop_name_kharif, Crop_name_rabi, Crop_name_other,Landuse,Soil_Type,Soil_Depth,Latitude,Longitude,Timestamp,Year,Yield_kharif,Yield_rabi,Yield_other,Watering_kharif,Watering_rabi,Watering_other,Watering_Type_kharif,Watering_Type_rabi,Watering_type_other,Image1,Image2, uploaded_by_uname, uploaded_by_fullname";
                appendLog(header);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile, true), StandardCharsets.UTF_8));
            buf.append(text);
            buf.newLine();
            buf.close();
            MediaScannerConnection.scanFile(this,
                    new String[]{logFile.toString()},
                    null,
                    null);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("ReadWriteFile", "Unable to write to the Final.csv file.");
            e.printStackTrace();
        } finally {

        }
    }


    private boolean validate_fields(EditText[] fields, MaterialSpinner[] dd_fields) {

        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
//                Toast.makeText(this, "Not Entered "+fields[i], Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        for (int i = 0; i < dd_fields.length; i++) {
            MaterialSpinner ms = dd_fields[i];
            String value = ms.getSelectedItem().toString();
            if (value.equals("-----")) {
                return false;
            }
        }

//        if((district.getSelectedItemPosition()==0) || (taluka.getSelectedItemPosition()==0) || (village.getSelectedItemPosition()==0) || (soil_type.getSelectedItemPosition()==0) || (crop_type.getSelectedItemPosition()==0) || (landuse_type.getSelectedItemPosition()==0)  )
//        {
//            return false;
//        }
        return true;
    }


    private void setTimestamp() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy---hh:mm:ss");
        String format = simpleDateFormat.format(new Date());

        TimeStmp.setText(format);
    }

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(Soil_Info.this, android.Manifest.permission.ACCESS_FINE_LOCATION))) {


            } else {
                ActivityCompat.requestPermissions(Soil_Info.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION

                        },
                        REQUEST_PERMISSIONS);

            }
        } else {
            boolean_permission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;

                } else {
                    Toast.makeText(getApplicationContext(), "कृपया परवानगी द्या", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = Double.valueOf(intent.getStringExtra("latutide"));
            longitude = Double.valueOf(intent.getStringExtra("longitude"));

            List<Address> addresses = null;


            Lat.setText(latitude + "");
            Longit.setText(longitude + "");
            location = String.valueOf(latitude)+","+String.valueOf(longitude);

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(GoogleService.str_receiver));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }


    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        verifyStoragePermissions(this);
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
    public void fetchFarmerNames(String census_code){
        if(census_code.equals("545833")){
            farmer_names = farmer_names_1;
            Farmer_name_1.setVisibility(View.GONE);
            Farmer_name.setVisibility(View.VISIBLE);
        }else{
//            farmer_names = new String[]{"testuserone","testusertwo"};
            Farmer_name_1.setVisibility(View.VISIBLE);
            Farmer_name.setVisibility(View.GONE);
        }
        if(true)
            return;
        RequestQueue queue = Volley.newRequestQueue(this);
        String URL =  "http://api-ffs.mahapocra.gov.in/3rd-party/authService/sso";
        JSONObject obj = new JSONObject();
        try {
            obj.put("census_code", census_code);
        }catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST,URL,obj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try{
                            JSONObject data = (JSONObject) response.get("data");
                            farmer_names = data.get("farmer_names").toString().split(",");

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            hideProgressDialog();
                        Log.d("volley error", error.toString());
                        Toast.makeText(getApplicationContext(), "Network ERROR", Toast.LENGTH_LONG).show();
                    }
                });
        queue.add(jsObjRequest);
    }
    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{permission},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
}
